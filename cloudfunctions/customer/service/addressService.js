// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { ADDRESS } = require('../config/tableConfig.js')
// 返回字段处理
const { ADDRESSFIELD } = require('../fields/addressField.js')

const getAddress = (address)=>{
  return model.query(ADDRESS, ADDRESSFIELD, address )
}
/**
 * 增加收货地址
 * @param {*} address 
 */
const createAddress = (address) => {
    return model.add(ADDRESS,address)
}
const updateAddress = (address, addressId) =>{
    return model.update(ADDRESS,address,addressId)
}
const deleteAddress = (addressId) =>{
  return model.remove(ADDRESS, addressId)
}
// 更新一列数据
const updateAddressColumn = (address, options) => {
  return model.updateCollection(ADDRESS, address, options )
}
// 导出
module.exports = {
  getAddress,
  createAddress,
  updateAddress,
  deleteAddress,
  updateAddressColumn
  

}
