// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { COUPON } = require('../config/tableConfig.js')

const { COUPONFIELD } = require('../fields/couponField.js')


const createCoupon = (coupon)=>{
    return model.add(COUPON, coupon)
}
const getCoupon = (coupon) => {
    return model.query(COUPON, COUPONFIELD, coupon )
}
const updateCoupon = (coupon, couponId) => {
  return model.update(COUPON, coupon, couponId)
}
const deleteCoupon = (couponId) => {
    return model.remove(COUPON, couponId)
}

  module.exports = {
    getCoupon,
    updateCoupon,
    createCoupon,
    deleteCoupon,
  }
  