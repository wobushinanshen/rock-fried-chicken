// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { CUSTOMER } = require('../config/tableConfig.js')

const { CUSTOMERFIELD } = require('../fields/customerField.js')


const createCustomer = (customer)=>{
    return model.add(CUSTOMER, customer)
}
const getCustomer = (customer) => {
    return model.query(CUSTOMER, CUSTOMERFIELD, customer )
}
const updateCustomer = (customer, customerId) => {
  return model.update(CUSTOMER, customer, customerId)
}
  module.exports = {
    getCustomer,
    updateCustomer,
    createCustomer,

  }
  