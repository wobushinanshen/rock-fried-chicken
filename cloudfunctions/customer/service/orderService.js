// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { ORDER } = require('../config/tableConfig.js')

const { ORDERFIELD } = require('../fields/orderField.js')


const createOrder = (order)=>{
    return model.add(ORDER, order)
}
const getOrder = (order1) => {
    return model.query(ORDER, ORDERFIELD, order1, page = 0, size = 100, order = { name: 'create_time', orderBy:'desc'} )
}
const updateOrder = (order, orderId) => {
  return model.update(ORDER, order, orderId)
}
const deleteOrder = (orderId) => {
    return model.remove(ORDER, orderId)
}
  module.exports = {
    getOrder,
    updateOrder,
    createOrder,
    deleteOrder,

  }
  