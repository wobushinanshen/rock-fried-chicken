// order 指定返回结果中记录需返回的字段
module.exports = {
    ORDERFIELD: {
        is_rider: true,
        comment: true,
        address:true,
        create_time: true,
        update_time:true,
        arrive_time:true,
        order_status: true,
        buyer_openid: true,
        order_id : true,
        total_price:true,
        products:true,
        selfget_status:true,
        print_status:true,
        shop_address:true,
        order_type:true,
        customer_name:true,
        customer_telephone:true,
        coupon_price:true,

    
    }
}
