// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router');
// 返回工具类
const returnUtil = require('utils/ReturnUtil.js')
// 商品信息业务层

const Customer = require('service/customerService.js')
const Coupon = require('service/couponService.js')
const Order = require('service/orderService.js')
const Address = require('service/addressService.js')

cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: 'chicken-shop'
  
})

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const app = new TcbRouter({ event });
  // app.use 表示该中间件会适用于所有的路由
  app.use(async (ctx, next) => {
    ctx.data = {};
    await next(); // 执行下一中间件
  });
  /***************************    顾客  *****************************************/    
 
  // 1、添加用户数据
  app.router('createCustomer',async (ctx,next) => {
    let customer = event.data.customer
    ctx.data = await Customer.createCustomer(customer)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 2、获取用户数据
  app.router('getCustomer',async (ctx,next) => {
    let customer = event.data.customer
    ctx.data = await Customer.getCustomer(customer)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 3、更新用户数据
  app.router('updateCustomer',async (ctx,next) => {
    let customer = event.data.customer
    let customerId = event.data.customerId
    ctx.data = await Customer.updateCustomer(customer, customerId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })


  /***************************    优惠券  *****************************************/    
 
  // 1、新建优惠券
  app.router('createCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    ctx.data = await Coupon.createCoupon(coupon)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 2、获取优惠券
  app.router('getCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    ctx.data = await Coupon.getCoupon(coupon)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 3、更新优惠券
  app.router('updateCoupon',async (ctx,next) => {
    let coupon = event.data.coupon
    let couponId = event.data.couponId
    ctx.data = await Coupon.updateCoupon(coupon, couponId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  // 4、删除优惠券
  app.router('deleteCoupon',async (ctx,next) => {
    let couponId = event.data.couponId
    ctx.data = await Coupon.deleteCoupon(couponId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })


  /************************** 订单 ******************/
  //  查询订单
  app.router('getOrder', async (ctx, next) => {
    let order = event.data.order
    ctx.data = await Order.getOrder(order)
    ctx.body = await returnUtil.success(ctx)
    await next();
  })
   //  添加订单
 app.router('createOrder', async (ctx, next) => {
    let order = event.data.order
    ctx.data = await Order.createOrder(order)
    ctx.body = await returnUtil.success(ctx)
    await next();
  })
   //  更新订单
 app.router('updateOrder', async (ctx, next) => {
    let order = event.data.order
    let orderId = event.data.orderId
    ctx.data = await Order.updateOrder(order, orderId)
    ctx.body = await returnUtil.success(ctx)
    await next();
  })
  // 删除订单
  app.router('deleteOrder', async (ctx, next) => {
    let orderId = event.data.orderId
    ctx.data = await Order.deleteOrder(orderId)
    ctx.body = await returnUtil.success(ctx)
    await next();
  })

/***************************    地址   *****************************************/    

  // 获取顾客收货地址列表
  app.router('getAddress',async (ctx,next) => {
    let address = event.data.address
    ctx.data = await Address.getAddress(address)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  // 添加收货地址
  app.router('createAddress',async (ctx,next) => {
    let address = event.data.address
    ctx.data = await Address.createAddress(address)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  // 修改地址
  app.router('updateAddress',async (ctx,next) => {
    let addressId = event.data.addressId
    let address = event.data.address
    ctx.data = await Address.updateAddress(address, addressId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

   // 更新一列地址信息 重置默认地址
   app.router('updateAddressColumn',async (ctx,next) => {
    let options = event.data.options
    let address = event.data.address
    ctx.data = await Address.updateAddressColumn(address, options)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
    // 删除地址
    app.router('deleteAddress',async (ctx,next) => {
    let addressId = event.data.addressId
    ctx.data = await Address.deleteAddress(addressId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })


  return app.serve();
}
