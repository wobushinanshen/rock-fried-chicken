// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router');
// 返回工具类
const ReturnUtil = require('utils/ReturnUtil.js')

const Notice = require('service/noticeService.js')
const Printer = require('service/printerService.js')
const Shop = require('service/shopService.js')
const RunTime = require('service/runTimeService.js')

cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: 'chicken-shop'
})
const IMAGEPREFIX = "https://6368-chicken-shop-1304148086.tcb.qcloud.la"

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const app = new TcbRouter({ event });
  // app.use 表示该中间件会适用于所有的路由
  app.use(async (ctx, next) => {
    ctx.data = {};
    await next(); // 执行下一中间件
  });

/***************************    notice   *****************************************/    

  app.router('createNotice', async(ctx, next) => {
    let notice1 = event.data.notice
    ctx.data = await Notice.createNotice(notice1)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })


   //获取公告
   app.router('getNotice',async (ctx,next) => {
    let notice = event.data.notice
    ctx.data = await Notice.getNotice(notice)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

  //更新公告
  app.router('updateNotice',async (ctx,next) => {
    let notice1 = event.data.notice
    let noticeId = event.data.noticeId
    console.log('notice',notice1)
    ctx.data = await Notice.updateNotice(notice1, noticeId)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })
  //删除公告
  app.router('deleteNotice',async (ctx,next) => {
    let noticeId = event.data.noticeId
    ctx.data = await Notice.deleteNotice(noticeId)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })
 
/***************************    打印机  *****************************************/    
    
    // 查询打印机
    app.router('getPrinter',async (ctx,next) => {
    let printer = event.data.printer
    ctx.data = await Printer.getPrinter(printer)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })
  // 更新打印机信息
  app.router('updatePrinter',async (ctx,next) => {
    let printer = event.data.printer
    let printerId = event.data.printerId
    ctx.data = await Printer.updatePrinter(printer, printerId)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })
  
  /***************************    shop  *****************************************/

   // 1、获取shop
   app.router('getShop',async (ctx,next) => {
    let shop = event.data.shop
    ctx.data = await Shop.getShop(shop)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

  app.router('updateShop',async (ctx,next) => {
    let shop = event.data.shop
    let shopId = event.data.shopId
    ctx.data = await Shop.updateShop(shop, shopId)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

  app.router('createShop', async(ctx, next) =>{
    let shop = event.data.shop
    ctx.data = await Shop.createShop(shop)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })

  app.router('deleteShop', async(ctx, next) =>{
    let shopId = event.data.shopId
    ctx.data = await Shop.deleteShop(shopId)
    ctx.body = await ReturnUtil.success(ctx)
    await next()
  })


   /************************** 营业时间 ******************/
   
  //  遍历营业时间
  app.router('getRunTime', async (ctx, next) => {
    let runtime = event.data.runtime
    ctx.data = await RunTime.getRunTime(runtime)
    ctx.body = await ReturnUtil.success(ctx)
    await next();
  })
   //  添加营业时间
 app.router('createRunTime', async (ctx, next) => {
  let runtime = event.data.runtime
  ctx.data = await RunTime.createRunTime(runtime)
  ctx.body = await ReturnUtil.success(ctx)
  await next();
})
   //  修改营业时间
 app.router('updateRunTime', async (ctx, next) => {
  let runtime = event.data.runtime
  let runtimeId = event.data.runtimeId
  ctx.data = await RunTime.updaterRunTime(runtime, runtimeId)
  ctx.body = await ReturnUtil.success(ctx)
  await next();
})
     //  删除营业时间
 app.router('deleteRunTime', async (ctx, next) => {
  let runtimeId = event.data.runtimeId
  ctx.data = await RunTime.deleteRunTime(runtimeId)
  ctx.body = await ReturnUtil.success(ctx)
  await next();
  })



return app.serve();
}
