module.exports = {
    SHOPFIELD: {
      admin_name: true,
      admin_password: true,
      shop_name: true,
      shop_status: true,
      shop_region: true,
      shop_radius: true,
      detail_address:true,
      shop_telephone: true,
      shop_description : true,
      shop_location:true,
      run_times: true,
      account:true,
      password:true,
      coupon_count:true,
      coupon_money:true,
      vip_fee:true,
      dispatch_fee:true,
      pack_fee:true,
      begin_time:true,
      end_time:true,
      
      
    }
  }