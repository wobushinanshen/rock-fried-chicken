// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { NOTICE } = require('../config/tableConfig.js')
// 返回字段处理
const { NOTICEFIELD } = require('../fields/noticeField.js')
/**
 * 获取首页轮播
 * @return 
 */
const getNotice = (notice)=>{
  return model.query(NOTICE, NOTICEFIELD, notice )
}
const createNotice = (notice) => {
    return model.add(NOTICE,notice)
}
const deleteNotice = (noticeId) => {
  return model.remove(NOTICE,noticeId)
}
const updateNotice = (notice, noticeId) => {
    
    return model.update(NOTICE,notice,noticeId)
}

// 导出
module.exports = {
    getNotice,
    deleteNotice,
    createNotice,
    updateNotice,

}
