// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { PRINTER } = require('../config/tableConfig.js')
// 返回字段处理
const { PRINTERFIELD } = require('../fields/printerField.js')


const createPrinter = (printer) => {
  return model.add(PRINTER, printer)
}
const getPrinter = (printer) => {
  return model.query(PRINTER, PRINTERFIELD, printer )
}
const updatePrinter = (printer, printerId) => {
    return model.update(PRINTER, printer, printerId)
}
const deletePrinter = (printerId) => {
  return model.remove(PRINTER, printerId)
}

// 导出
module.exports = {
    getPrinter,
    updatePrinter,
    createPrinter,
    deletePrinter,


}
