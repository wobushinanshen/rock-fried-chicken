// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { RUNTIME } = require('../config/tableConfig.js')

const { RUNTIMEFIELD } = require('../fields/runTimeField.js')


/**
 * 获取首页轮播
 * @return 
 */
const getRunTime = (runTime) => {
    return model.query(RUNTIME, RUNTIMEFIELD, runTime )
  // 导出
}
const updateRunTime = (runTime, runTimeId) => {
  return model.update(RUNTIME, runTime, runTimeId)
}
const createRunTime = (runTime) => {
  return model.add(RUNTIME, runTime)
}
const deleteRunTime = (runTimeId) => {
    return model.remove(RUNTIME, runTimeId)
  }

  module.exports = {
    getRunTime,
    updateRunTime,
    createRunTime,
    deleteRunTime,


  }
  