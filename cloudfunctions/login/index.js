// 云函数模板
// 部署：在 cloud-functions/login 文件夹右击选择 “上传并部署”

const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
const ReturnUtil = require('utils/ReturnUtil.js')
// 初始化 cloud
cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: 'chicken-shop'
  
})

/**
 * 这个示例将经自动鉴权过的小程序用户 openid 返回给小程序端
 * 
 * event 参数包含小程序端调用传入的 data
 * 
 */
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const app = new TcbRouter({event});

     // app.use 表示该中间件会适用于所有的路由
     app.use(async (ctx, next) => {
      ctx.data = {};
      await next(); // 执行下一中间件
  });

  app.router('getOpenId', async (ctx, next) => {
      ctx.data = wxContext.OPENID
      ctx.body = ReturnUtil.success(ctx)
      await next(); // 执行下一中间件
  });

  app.router('getTelephone', async (ctx, next) => {
    console.log('event',event)
    ctx.data = {
      ...event,
      ...wxContext
    }
    ctx.body = ReturnUtil.success(ctx)
    await next(); // 执行下一中间件
});


  return app.serve();

  // 可执行其他自定义逻辑
  // console.log 的内容可以在云开发云函数调用日志查看

  // 获取 WX Context (微信调用上下文)，包括 OPENID、APPID、及 UNIONID（需满足 UNIONID 获取条件）等信息
  

  
}
// exports.main = async (event) => { return { ...event, ...cloud.getWXContext() } }

