// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router');
// 返回工具类
const returnUtil = require('utils/ReturnUtil.js')


const Product = require('service/productService.js')  
const Banner = require('service/bannerService.js')
// const Theme  = require('service/themeService.js')
const Category = require('service/categoryService.js');



cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: cloud.DYNAMIC_CURRENT_ENV
})
const IMAGEPREFIX = "cloud://chicken-shop.6368-chicken-shop-1304148086"

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const app = new TcbRouter({ event });
  // app.use 表示该中间件会适用于所有的路由
  app.use(async (ctx, next) => {
    ctx.data = {};
    await next(); // 执行下一中间件
  });


// 轮播图片地址拼接
 function _bannerItem(data){
  return new Promise((resolve,reject) => {
    data.then(res=>{
      res.data.forEach(data => {
        data.image = IMAGEPREFIX + data.image
      })
      resolve(res)
    })
  }) 
}
// 主题图片地址拼接
function _themeItem(data){
  return new Promise((resolve,reject) => {
    data.then(res=>{
      res.data.forEach(data => {
        data.theme_icon = IMAGEPREFIX + data.theme_icon
      })
      resolve(res)
    })
  }) 
}
// 多个商品图片地址拼接
function _productImgs(data){
  return new Promise((resolve,reject) => {
    data.then(res=>{
      let products = res.data 
      for(let i=0;i<products.length;i++){
        products[i].product_image = IMAGEPREFIX + products[i].product_image
      }
      resolve(res)
    })
  }) 
}

function _spliceImg(array){
  for(let i=0;i<array.length;i++){
    array[i] = IMAGEPREFIX + array[i]
  }
}

  // 单个商品图片地址拼接
  function _productImg(data) {
   return new Promise((resolve, reject) => {
     data.then(res => {
        res.data.product_image = IMAGEPREFIX + res.data.product_image
       resolve(res)
     })
   })
 }
/***************************    banner   *****************************************/    

  app.router('getBanner',async(ctx, next)=>{
    ctx.data = await _bannerItem(Banner.getBanner())
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  app.router('addBanner',async(ctx, next)=>{
    let banner1 = event.data.banner
    ctx.data = await Banner.addBanner(banner1)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  app.router('deleteBanner',async(ctx, next)=>{
    let bannerId = event.data.bannerId
    ctx.data = await Banner.deleteBanner(bannerId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
 

 /************************** 商品 **************************/
  app.router('getProduct',async (ctx,next) => {
    let product = event.data.product
    ctx.data = await _productImgs(Product.getProduct(product))
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  //修改商品
  app.router('updateProduct',async (ctx,next) => {
    let product1 = event.data.product
    let productId = event.data.productId
    console.log('p1',product1)
    ctx.data = await Product.updateProduct(product1,productId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  
   app.router('createProduct',async (ctx, next) => {
    let product =  event.data.product
    ctx.data = await Product.createProduct(product)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  //模糊查询商铺
  app.router('regQuery',async (ctx,next) => {
    let regexp = event.data.regexp
    let columns = event.data.columns
    ctx.data = await _productItem(Product.regQuery(regexp, columns))
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  //删除商品
  app.router('deleteProduct',async (ctx,next) => {
    let productId = event.data.productId
    ctx.data = await Product.deleteProduct(productId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

 /************************** 分类 **************************/
 
  app.router('createCategory',async (ctx,next) => {
    let category = event.data.category
    ctx.data = await Category.createCategory(category)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })

  app.router('getCategory',async (ctx,next) => {
    let category = event.data.category
    ctx.data = await Category.getCategory(category)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  // 添加商品种类
  app.router('createCategory',async (ctx,next) => {
    let category = event.data.category
    ctx.data = await Category.createCategory(category)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
  // 删除商品种类
  app.router('deleteCategory',async (ctx,next) => {
    let categoryId = event.data.categoryId
    ctx.data = await Category.deleteCategory(categoryId)
    ctx.body = await returnUtil.success(ctx)
    await next()
  })
 
    
    
//  // 获取最新前5商品
//  app.router('getProductNew',async (ctx,next) => {
//    // Product.getProduct({},0,4)  {}：没有条件直接控{}，0，4 分页查询 
//    ctx.data = await _productItem(Product.getProduct({},0,4))
//    ctx.body = await returnUtil.success(ctx)
//    await next()
//  })

  





return app.serve();
}
