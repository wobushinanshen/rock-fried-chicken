// 导入数据库操作公共方法
const model = require('../models/BaseModel.js')
// 全局集合名称
const { PRODUCT_CATEGORY } = require('../config/tableConfig.js')
// 返回字段处理
const { PRODUCT_CATEGORY_FIELD } = require('../fields/productCategoryField.js')

const createCategory = (category)=>{
    return model.add(PRODUCT_CATEGORY,category)
}
const deleteCategory = (categoryId)=>{
    return model.remove(PRODUCT_CATEGORY,categoryId)
}
const updateCategory = (category, categoryId)=>{
    return model.update(PRODUCT_CATEGORY, category, categoryId)
}
const getCategory = (category) =>{
    return model.query(PRODUCT_CATEGORY, PRODUCT_CATEGORY_FIELD, category)
}
module.exports = {
    createCategory,
    deleteCategory,
    getCategory,
    updateCategory,
}