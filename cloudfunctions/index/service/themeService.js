// 引入BaseModel 集合名  字段过滤
const model = require('../models/BaseModel.js')
const { THEME } = require('../config/tableConfig.js')
const { ORDER } = require('../config/tableConfig.js')
const { THEMEFIELD } = require('../fields/themeField.js')

/**
 * 获取主题列表
 * @return 
 */
const getTheme = () => {
  return model.query( THEME, THEMEFIELD )
}
const createTheme = (theme) => {
  return model.add(THEME,theme)
}
const deleteTheme = (theme_id) => {
  return model.remove(THEME,theme_id)
}

module.exports = {
  getTheme,
  createTheme,
  deleteTheme,
}
