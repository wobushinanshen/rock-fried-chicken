const model = require('../models/BaseModel.js')
const { PRODUCTFIELD } = require('../fields/productField.js')

// 在原来的上面增加 PRODUCT_CATEGORY
const { PRODUCT ,PRODUCT_CATEGORY } = require('../config/tableConfig.js')
// 新增分类字段过滤
const { PRODUCT_CATEGORY_FIELD } = require('../fields/productCategoryField.js')




/**
 * 获取商品
 * @param options 条件
 * @param page    
 * @param size
 * @return 
 */
const getProduct = (product ) => {
  return model.query(PRODUCT, PRODUCTFIELD, product)
}

/**
 * 新增商品
 */
const createProduct=(product)=>{
  return model.add(PRODUCT, product)
}
/**
 *  更新商品
 * @param {*} product 
 * @param {*} productId 
 */
const updateProduct = (product,productId)=> {
  return model.update(PRODUCT, product,productId)
}

/**
 * 删除商品
 * @param {r} productId 
 */
const deleteProduct = (productId)=> {
  return model.remove(PRODUCT,productId)
}

const regQuery = (regexp, columns) => {
  return model.regexpQuery(regexp, PRODUCT, columns[0], columns[1])
}

module.exports = {
  getProduct,
  createProduct,
  updateProduct,
  deleteProduct,
  regQuery
}
