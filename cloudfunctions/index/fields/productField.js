// product 指定返回结果中记录需返回的字段
module.exports = {
  PRODUCTFIELD: {
    product_name: true,
    product_image: true,
    product_price: true,
    product_sell_price: true,
    product_status: true,
    product_description:true,
    product_tag:true,
    category_type:true,
    product_sales:true,
    // create_time:false,
    // update_time:false,
  
  }
}
