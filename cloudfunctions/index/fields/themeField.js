// theme 指定返回结果中记录需返回的字段 _id需要特殊处理
module.exports = {
  THEMEFIELD: {
    theme_icon: true,
    theme_name: true,
    theme_type: true,
    _id: false 
  }
}
