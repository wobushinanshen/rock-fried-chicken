// app.js
import { LoginModel } from "models/LoginModel.js";
// import { RunTimeModel } from "models/RunTimeModel.js"
let Login = new LoginModel()
// let RunTime = new RunTimeModel()
App({
  onLaunch: function () {
  
     
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({

        // env 参数说明：
        //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        //   如不填则使用默认环境（第一个创建的环境）
        // env: 'my-env-id',
        traceUser: true,
      })
    }

    this._getOpenId()

    this.globalData = {}
  },
  onload:function(){
    
  },
  _getOpenId:async function(){
    // let that = this
    // wx.cloud.callFunction({
    //   name: 'login',
    // }).then(res => {
    //   let openId = res.result.event.userInfo.openId
    //    that.globalData.openId = openId
    // }).catch(err => {
    
    // })  
    let res = await Login.getOpenId()
    this.globalData.openId = res.result.data
    console.log('res',res.result.data)
  },
  // 获取店铺信息 以及判断店铺状态：是否营业
  // _getShopInfo:async function(){
  //   let shopInfo = await Shop.getShop({})
  //   let status = shopInfo[0].shop_status
  //   if(status){
  //     status = await RunTime.checkShopOpen()
  //   }
  //   shopInfo[0].shop_status = status
  //   this.globalData.shopInfo = shopInfo[0]
  //   console.log('是佛业营业',shopInfo)
  // } 

})
