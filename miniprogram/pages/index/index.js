import { BannerModel } from '../../models/BannerModel.js'
import { ShopModel } from '../../models/ShopModel.js'
import { RunTimeModel } from '../../models/RunTimeModel.js'
import { OrderModel } from '../../models/OrdelModel.js'
import { LoginModel } from '../../models/LoginModel.js'
const AUTO = require('../../utils/auth.js')
// 使用前需要实例化才能使用
let Banner = new BannerModel()
let Shop = new ShopModel()
let RunTime = new RunTimeModel()
let Order  = new OrderModel()
let Login = new LoginModel()
var  App =getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //自动轮播
    interval: 3000, // 自动切换时间间隔
    duration: 1000, // 滑动动画时长
    circular: true,//是否采用衔接滑动 
    banners:[],
    shop:{},//店铺信息
    shopStatus:false,//店铺状态
    card1:{title:'外卖',desc:'快递速达',imagePath:'../../images/index/shop.png'},
    card2:{title:'收银',desc:'当面收款',imagePath:'../../images/index/pos.png'},
    // 店铺地址 经纬度
    location:{
      latitude: '30.253778',
      longitude: '120.16097',
    },
    name: '西湖音乐喷泉表演',
    desc: '湖滨路与平海路交汇处西',
    // 是否授权登录
    wxlogin:true,
    // 营业时间
    runTimes:[]
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._init()
  },

  // 初始化
  _init:function(){ 
    // 1、获取banner
    this._getBanner()
    // 2、获取商店信息
    this._getShopInfo()
    // 3、获取营业时间
    this._getRunTime()
    // 4、校验是否授权
    this._checkLogin()
    this.getOpenId()
  },
  getPhoneNumber:async function (e) {
    console.log('e',e)
    // 点击 拒绝
    if(e.detail.errMsg !== 'getPhoneNumber:ok'){
      wx.showToast({
        title: '拒绝',
      })
      return
    }


    let  cloudID = e.detail.cloudID
 
    let res = await Login.getTelephone(cloudID)
   
    let telephone = res.result.data.weRunData.data.phoneNumber
    console.log('gettelephone',telephone)
   
  },
  getOpenId: async function(){
    let res = await Login.getTelephone()
    console.log('openid',res)
  },

  // 弹出授权界面 ：点击取消授权
  cancelLogin:function () {
    this.setData({
      wxlogin: true
    }) 
  } ,
  // 弹出授权界面 ：点击授权登录
  processLogin:function(){
    this.cancelLogin()
  },
  // 判断是否已经登录
  _checkLogin:function(){
    AUTO.checkLogin().then(res=>{
      this.setData({
        wxlogin:res
      })
    })
  },
   
  // 轮播图 
  _getBanner:async function(){
    let res = await Banner.getBanner({})
    console.log('banner',res)
    this.setData({
      banners :  res
    })
  },
  // 获取店铺信息 并检查店铺状态
  _getShopInfo:async function(){
    let res = await Shop.getShop({})
    this.setData({
      shop:res[0],
      location:res[0].shop_location
    })
    this._checkShopStatus()
  },
  // 获取店铺营业时间
  _getRunTime:async function(){
    let res  = await RunTime.getRunTime({})
    this.setData({
      runTimes:res
    })
  },
  // 检查店铺状态 在_getShopInfo方法后执行
  _checkShopStatus:async function(){
    // 1、先判断店铺状态是否为：正常营业
    let status = this.data.shop.shop_status
    // 2、如果正常营业 在根据营业时间判断店铺：是否营业
    if(status){
      status = await RunTime.checkShopOpen()
    }
    this.setData({
      shopStatus:status
    })
    App.globalData.shopStatus = status
  },

  // 外卖图标
  _toCategory:function(){
    wx.switchTab({
      url: '/pages/category/category',
    })
  },
  // 收银图标
  _toPay:function(){
    wx.navigateTo({
      url: '/pages/toPay/toPay',
    })
  },
  
  /**
   * 下拉刷新
   */
  onPullDownRefresh: function () {
    this.onLoad()
    wx.stopPullDownRefresh()
  },
  // 点击导航
  clickAddress:function(){
    let location = this.data.location
    wx.openLocation({
      latitude: location.latitude,
      longitude: location.longitude,
    })
  },
  // 点击导航
  clickTelephone:function(){
    let telephone = this.data.shop.shop_telephone
    wx.makePhoneCall({
      phoneNumber: telephone,
    })
  },
  
})
 
