// pages/my/my.js
import { OrderModel } from '../../models/OrdelModel.js'
let Order = new OrderModel()
var App = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    orders: [],
    doingOrder:[],
    selfGetOrder:[],
    takeOutOrder:[],
    completedOrder:[],
    openId: "",
    order:{},
    printer:{},
    identity:false,
    
  },
  // 初始化
  _init: function () {
    this._getOrders()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onLoad:function (options) {
    if(options.active){
      this.setData({
        active:parseInt(options.active)
      })
    }
    this._init()
  },
  onShow:function(){
    this._init()
  },
  // 获取 所有进行中的订单
  _getOrders:async function(){
    let order = {
      buyer_openid:this._getOpenId()
    }
    let result = await Order.getOrder(order)
    console.log('order',result)
    if(result.length > 0){
      this.setData({
        orders:result
      })
      this._classifyOrders(result)
    }
  },

  // 调用云函数获取openId
  _getOpenId:function(){
    let openId = App.globalData.openId
    this.setData({openId})
    return openId
  },
  // 订单分类
  _classifyOrders:function(orders){
    let doingOrder = []
    let selfGetOrder = []
    let takeOutOrder = []
    let completedOrder = []
    for(var i in orders){
      switch(orders[i].order_status){
        case 0:
          doingOrder.push(orders[i])
          break;
        case 1:
          {
            if(orders[i].order_type == 1){
              selfGetOrder.push(orders[i])
            }else if(orders[i].order_type == 2){
              takeOutOrder.push(orders[i])
            }
            break;
          }
        case 2:
          completedOrder.push(orders[i])
          break;
        default:
          break
      }
    }
    this.setData({
      doingOrder: doingOrder,
      selfGetOrder: selfGetOrder,
      takeOutOrder:takeOutOrder,
      completedOrder: completedOrder
    })
  },
  // 根据订单状态更新订单
  confirm:async function(e){
    let status = parseInt(e.detail.order.order_status)
    let order = {
      order_status:status + 1
    }
    if(status == 0){
      return
    }
    // 如果订单状态为：未完成状态 点击按钮则更新订单状态 status = status + 1
    if(status == 1){
      await Order.updateOrder(order, e.detail.order._id)
      this.onShow()
    }
    // 如果订单状态为：已完成 则删除订单
    if(status == 2){
      let res = await wx.showModal({
        title:'提示',
        content:'确认删除该订单?'
      })
      if(res.confirm){
        await Order.deleteOrder(e.detail.order._id)
        this.onShow()
      }
      
    }
  },
  // 订单详情
  orderDetail:function (e) {
    let orderId = e.detail.orderId
    wx.navigateTo({
      url: '/pages/orderDetail/orderDetail?orderId='+orderId,
    })
  },
  // 生成订单小票格式
  _orderContent:function(orderData){
    let date = ''+new Date().getTime()
    let orderNumber = date.substring(9)
    var strA = '.';
    var strB = '*';
    var content = "<FS2><center>#订单号："+orderNumber+"</center></FS2>";
    content += "<FS2><center>*微信小程序*</center></FS2>";
    content += "<FS2><center>*摇滚炒鸡*</center></FS2>";
    content += strA.repeat( 32);
    content += "<FS2><center>--在线支付--</center></FS2>";
    
    content += "订单时间:"+myDate.getDate(orderData.create_time)+"\n";
    content += "订单编号:"+orderData.order_id+"\n";
    content += strB.repeat( 14) + "商品" + strB.repeat( 14);
    content += "<table>";
    for(let i=0;i<orderData.products.length;i++){
      content += "<tr><td>"+orderData.products[i].product_name+"</td><td>"+orderData.products[i].product_count+"</td><td>"+orderData.products[i].product_sell_price+"</td></tr>";
    }
    content += "</table>";
    content += strA.repeat( 32);
    content += "<QR>摇滚炒鸡</QR>";
    content += "小计:￥"+orderData.total_price+"\n";
    content += "折扣:￥0 \n";
    content += strB.repeat(32);
    content += "<FS2>订单总价:￥"+orderData.total_price+"</FS2>\n";
    content += strB.repeat(32);
    content += "<FS2>"+orderData.address.detail+"</FS2>\n";
    content += "<FS2>手机号：</FS2>\n";
    content += "<FS2>"+orderData.address.telephone+"</FS2>\n";
    content += "<FS2>预定时间: </FS2>\n";
    content += "<FS2>"+orderData.arrive_time+"</FS2>\n";
    content += strA.repeat( 32);
    content += "<FS2>备注："+orderData.comment+"</FS2>\n";
    content += "<FS2><center>**#1 完**</center></FS2>";
  
    return content
},
  selfget:async function(){
    let address={
      telephone:'13253377401',
      name:'张彪'
    }
    let products=[
      {
        product_name:'土豆丝'
      },
      {
        product_name:'娃娃菜'
      },
      {
        product_name:'咸鸭蛋'
      }
    ]
    let order ={
      address:address,
      products:products,
      order_status:1,
      order_type:2,
      create_time:new Date()
    }
    let result = await Order.createOrder(order)
    console.log('create',result)
  },
  takeout: async function(){
    let address={
     
    }
    let products=[
      {
        product_name:'土豆丝'
      },
      {
        product_name:'娃娃菜'
      },
      {
        product_name:'咸鸭蛋'
      }
    ]
    let order ={
      address:address,
      products:products,
      order_status:1,
      order_type:1,
      customer_telephone:13253377401,
      customer_name:'张彪',
      create_time:new Date()
    }
    let result = await Order.createOrder(order)
    console.log('create',result)
  },

  // 打印订单
  printOrder:function(e){
    
    let that = this
    let orderId = e.detail.orderId
    console.log('printOrder',orderId)
    Order.getOrderById(orderId, res=>{
   
      let order = res.result.data.data
      console.log('order',order)
      let content =  that._orderContent(order)
      let machineCode = that.data.printer.machine_code
      let orderId2 = '2020'+new Date().getTime()
      Print.toPrint(machineCode,orderId2,content)
      let newOrder = {print_status:true}
      Order.updateOrder(orderId, newOrder, res=>{
        console.log('更新打印状态成功',res)
        that.onShow()
      })
    })
  },
 
})
