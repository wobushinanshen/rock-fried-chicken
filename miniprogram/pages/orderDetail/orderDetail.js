import {  OrderModel } from '../../models/OrdelModel.js'
// import { MyDate } from '../../models/MyDate.js'
// let myDate = new MyDate()
let Order = new OrderModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        openId:'',
        orderId:'',
        orderData:{},
        orderTime:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let orderId = options.orderId
        this._init(orderId)
    },
    copyOrderNumber:function(){
        wx.setClipboardData({
            data: 'data',
            success (res) {
              wx.getClipboardData({
                success (res) {
                  console.log(res.data) // data
                }
              })
            }
          })
    },
    _init:function (orderId) {
        this._getOrders(orderId)
    },
    _getOrders:function(orderId) {
        Order.getOrderById(orderId, res=>{
            let orderData = res.result.data.data
            let orderTime = myDate.getDate(orderData.create_time)
            this.setData({orderData,orderTime})
            console.log('order',orderData)

        })
        
    },
     // 调用云函数获取openId,并获取默认地址
     _getUserAddress:function(){
        let that = this
        wx.cloud.callFunction({
        name: 'login',
        }).then(res => {
          let openId = res.result.event.userInfo.openId
          this.setData({openId})
          Address.getDefaultAddress(openId, res=>{
            let addresses = res.result.data.data
            if(addresses.length > 0){
              that.setData({
                userAddress:addresses[0],
                haveAddress:true
              })
            }else{
              this.setData({
                haveAddress:false
              })
            }
           
            })
          }).catch(err => {
        })
      },
   
})