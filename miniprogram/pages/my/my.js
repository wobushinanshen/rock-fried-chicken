// pages/my/my.js
const AUTO = require('../../utils/auth.js')
import { CustomerModel } from '../../models/CustomerModel.js'
import { CouponModel } from '../../models/CouponModel.js'
let Customer = new CustomerModel()
let App = getApp()
let Coupon = new CouponModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo:{},//用户信息
        login:false,//用户是否已授权登录
        openId:'',
        customer:{},
        isVip:false,
        coupons:''

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      // this._getUserInfo()
    },
    onShow:function(){
      this._getUserInfo()
    },
    _getOpenId:function(){
      let openId = App.globalData.openId
      this.setData({openId})
      return openId
    },
    _getCouponCounts: async function(){
      let result = await Coupon.getUsefulCoupons()
      if(result.length > 0){
        this.setData({
          coupons:result.length
        })
      }else{
        this.setData({
          coupons:0
        })
      }
    },
    // 新增用户记录
    _createCustomer: async function(){
      let customer = {
        openid : this.data.openId,
        balance : 0,
        growth : 0,
        score : 0,
        sign_in:'',
        vip:false,
        teltphone:'',
        username:'',
      }
      this.setData({
        customer:customer
      })
      await Customer.createCustomer(customer)
    },

    _getUserInfo:async function(){
      await this._getOpenId()
      // 1、判断是否授权
      let result = await wx.getSetting()
      //如果已授权
      if(result.authSetting['scope.userInfo']){
        //2、获取用户头像和微信名称
        let res = await wx.getUserInfo({})
        this.setData({
          userInfo:res.userInfo,
          login:true,
      })
        // 3、判断用户信息是否写到数据库中
        let params = {
          openid:this.data.openId
        } 
        let customer =  await Customer.getCustomer(params)
        console.log('customer',customer)
        // 如果数据库中没有用户数据 则为用户创建新纪录
        if(customer.length <= 0){
          await this._createCustomer()
        // 如果数据库中存在用户数据 则直接读取
        }else{
          this.setData({
            vip: customer[0].vip,
            customer:customer[0]
          })
        }
        // 最后获取用户优惠券数量
        this._getCouponCounts()

    }else{
      this.setData({
        login:false
      })
    }
      
  },
  _authLogin:function(e){
    this.onShow()
  },
  
 
  goOrder:function(e){
    let active = e.currentTarget.dataset.type
    wx.switchTab({
      url: '/pages/orders/orders',
    })
   
  },
  // 我的订单
  myOrders:function(){
    
    wx.navigateTo({
      url: '/pages/orders/orders',
    })
  },
  // 订单自取
  selfGetOrders:function(){
    wx.navigateTo({
      url: '/pages/selfGetOrders/selfGetOrders?from='+'customer',
    })
  },
  // 每日签到
  signIn:function(){
    wx.navigateTo({
      url: '/pages/sign/sign',
    })
  },
  // 积分商城
  pointShop:function(){
    wx.navigateTo({
      url: '/pages/pointShop/pointShop',
    })
  },
  // 地址管理
  myAddress:function(){
    wx.navigateTo({
      url: '/pages/address/addressList/addressList',
    })
  },
  // 关于我们
  aboutUs:function(){
    wx.navigateTo({
      url: '/pages/about/index',
    })
  },
  // 商家入口
  adminPage:function(){
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },
  // 帮助
  help:function(){
    wx.navigateTo({
      url: '/pages/help/index',
    })
  },
  // 
  vocation:function(){
    wx.navigateTo({
      url: '/pages/leave/index/index',
    })
  },
  // 优惠券
  myCoupon:function(){
    wx.navigateTo({
      url: '/pages/coupon/coupon',
    })
  },
  // 会员
  myVip:function(){
    wx.navigateTo({
      url: '/pages/vip/vip',
    })
  },



  
})