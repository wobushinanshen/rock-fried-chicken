import { ProductModel } from '../../models/ProductModel.js'
let Product = new ProductModel()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        search:'',
        products:[],
        product:{},
        show:false

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let search = options.search
        let colunms = ['product_name','product_tag']
        Product.regQuery(search, colunms, res=>{
          let products = res.result.data.data
          this.setData({products})
          console.log('e',products)
        })
    },
    onShow:function(){
      this.setData({
        show:false
      })
    },
    // 点击购物车图标：弹出框
  addCart:function(e){
    let show = e.detail.show
    let product = e.detail.product
    console.log('e',e.detail)
    this.setData({
      show:show,
      product:product
    })
  },
  // 加入购物车
  joinCart:function(e){
    let count = e.detail.count
    this.setData({
      show:true
    })
    Cart.add(this.data.product,count,res=>{
      this.setData({
        show:false
      })
      wx.showToast({
        icon:"success",
        context:"添加成功",
        title:'添加成功'
      })
    })
  },
  // 弹出框，立即购买
  buy:function(e){
    let count = e.detail.count
    wx.navigateTo({
      url: '../order/order?count='+count+'&from=product'+'&productId='+this.data.product._id,
    })
  },
  
  // 跳转商品详情
  showDetail:function(e){
    console.log('navigateto')
    wx.navigateTo({
      url: '/pages/product/product?product_id=' + e.detail.productId,
    })
  },
    
})