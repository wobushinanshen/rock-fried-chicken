let DateUtil = require('../../utils/dateUtil.js')
import { CouponModel } from '../../models/CouponModel.js'
let Coupon = new CouponModel()
let App = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        radio: '1',
        beginDay: '2016-09-01',
        endDay:'2016-09-01',
        time: '12:01',
        couponName:'',
        subPrice:'',
        sumPrice:'',
        coupons:[],
        date:'',
        openId:'',
        active:1,
        coupons:[]
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
        
    },
    _init:function(){
        this._getCoupons()

    },
    _getOpenId:function(){
        let openId = App.globalData.openId
        this.setData({openId})
        return openId
      },
    _getDay:function(){
       console.log('day',DateUtil.getDate('yyyy-MM-dd hh:mm:ss',30)) 
    },
    _getCoupons: async function(){
        let openId = this._getOpenId()
        let coupon = {
            openid : openId
        }
        let result = await Coupon.getUsefulCoupons()
        console.log(result)
        if(result.length > 0){
            this.setData({
                coupons:result
            })
        }
    },  
   
   


   
})