// pages/comment/comment.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        sumCount:0,
        comment:'',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
       
    },
    // 输入备注
    input: function(e) {
        let comment = e.detail.value
        if(comment.length == 50){
            wx.showToast({
                title: '最多输入50个字',
                icon:'none'
              })
        }
        this.setData({
            comment:comment,
            sumCount:comment.length
        })
       
      },
    // 点击快捷标签 将内容添加到备注中
    addTab:function(e){
        let tab = e.currentTarget.dataset.tab
        let comment = this.data.comment
        if((comment+tab).length > 50){
            wx.showToast({
              title: '不能超过50个字',
              icon:'none'
            })
        }else{
            comment+=tab
            this.setData({
                comment:comment,
                sumCount:comment.length
            })
        }
    },
    // 点击取消
    cancel:function(){
        wx.navigateBack({
          delta: 1,
        })
    },
    // 点击确认
    confirm:function(){
        let comment = this.data.comment
        // 1、清理旧缓存
        try {
            wx.removeStorageSync('comment')
          } catch (e) {
            // Do something when catch error
          }
        // 2、将新的备注信息存储到缓存中
        try {
            wx.setStorageSync('comment', comment)
          } catch (e) { }

        this.cancel()
        
    },

  
})