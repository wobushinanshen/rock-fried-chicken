const  Payment  = require('../../utils/payment.js')
const  { Util } = require('../../utils/util.js') 
let util = new Util()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        money:0.00
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    _payGo:function(totalPrice){
        let that = this
        let order = {
          orderId : util.uuid2(32,32),
          body : '店内支付',
          nonceStr : util.uuid2(32,32),
          money : totalPrice * 100,
        }
        Payment.unifiedOrder(order, res=>{
          console.log("获取支付参数成功", res)
          const payment = res.result.data.payment
          //调起支付
          wx.requestPayment({
            ...payment,
            success(res) {
              console.log('支付成功', res)
              wx.switchTab({
                url: '/pages/index/index',
              })
            },
            fail(res) {
              wx.showToast({
                title: '订单支付失败',
                icon:'none'
              })
              console.error('支付失败', res)
            }
          })
        })
      },
    input:function(e){
        let money = e.detail.value
        if(money.length > 0){
            money = parseFloat(money).toFixed(2)
        }else{
            money = 0.00
        }
        this.setData({money})
        console.log(money)
    },
    _isPrice:function(price){
        var priceReg = /(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/
        if(priceReg.test(price)){
            return true
        }else{
            return false
        }
    },
    pay:function(){
        let money = this.data.money
        // 对输入的金额进行校验
        if(!this._isPrice(money)){
            wx.showToast({
              title: '请输入合法金额!',
              icon:"none"
            })
        }else{
            this._payGo(money)
        }
    }
})