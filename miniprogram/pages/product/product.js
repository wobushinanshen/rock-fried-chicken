// pages/product/product.js
import {  ProductModel } from '../../models/ProductModel.js'
// import { CartModel } from '../../models/CartModel.js'
import { NoticeModel } from '../../models/NoticeModel.js'
let Notice = new NoticeModel()
let Product = new ProductModel()
// let Cart = new CartModel()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //自动轮播
    interval: 3000, // 自动切换时间间隔
    duration: 1000, // 滑动动画时长
    circular: true,//是否采用衔接滑动 
    productId :'',
    currentTab: 0,      // tab选项卡
    product:{},
    counts:0,
    show:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let product_id = options.product_id
    this._init(product_id)
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  _onClose() {
    this.setData({ show: false });
  },
  _showPupop:function(){
    this.setData({
      show:true
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      show:false
    })
  },
  setCount:function(event){
    let counts  = event.detail

    this.setData({
      counts:counts
    })
    console.log('e',this.data.counts)
  },
  
  // 详情和参数切换
  clickTab: function (e) {
    const currentTab = e.currentTarget.dataset.current
    this.setData({
      currentTab
    })
  },
  _init:function(product_id){
    // 获取商品信息
    Product.getProductById(product_id,res=>{
     
      this.setData({
        product:res.result.data.data
      })
      console.log('product',this.data.product.product_imgs)

    })
    Notice.getNotice(res=>{
      console.log('查询成功',res)
      this.setData({
          courierFee:res.result.data.data[0].courier_fee,
          isFee:res.result.data.data[0].is_fee
      })
  })

  },
  // 购物车
  goCart:function(){   
    wx.switchTab({    
      url: '/pages/cart/cart',
    })
  },
  // 回到首页
  goHome:function(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  // 关注
  focus:function(e){
      wx.showToast({
        icon: 'none',
        title: '暂未开通'
      })
  },
   // 添加到购物车
  joinCart:function(e){
    let count = e.detail.count
    this.setData({
      show:true
    })
    Cart.add(this.data.product,count,res=>{
      this.setData({
        show:false
      })
      wx.showToast({
        icon:"success",
        context:"添加成功",
        title:'添加成功'
      })
    })
  },
  buy:function(e){
    let count = e.detail.count
    wx.navigateTo({
      url: '../order/order?count='+count+'&from=product'+'&productId='+this.data.product._id,
    })
  },
  // 立即购买
  immediately:function(){
    let that = this
    // 数量默认为1
    let count = 1
    wx.navigateTo({
      url: '../order/order?count=' + count + '&productId='+that.data.product._id+ '&from=product'
    });
  }
})