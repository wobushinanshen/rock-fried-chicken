// pages/my/my.js
import {OrderModel} from '../../models/OrdelModel.js'
import { Toast } from '../../components/dist/toast/toast'
const Print = require('../../utils/print.js')
import { PrinterModel } from '../../models/PrinterModel.js'
let  Printer = new PrinterModel()
let Order = new OrderModel();
var App = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    orders: [],
    allOrder:false,
    waitingOrder:[],
    acceptedOrder:[],
    waitGetOrder:[],
    finshedOrder:[],
    openId: "",
    order:{},
    printer:{},
    identity:false,

  },
  // 初始化
  _init: function () {
    this._getOrders()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onLoad:function (options) {
    this._init()
  },
  onShow:function(){
    this._init()
  },
  // 调用云函数获取openId
  _getOpenId:function(){
    let openId = App.globalData.openId
    this.setData({openId})
    return openId
  },
  
  // 用户获取订单
  _getOrders:function(){
    let that = this
    let openId = this._getOpenId()
    let order = {
      buyer_openid: openId,
      is_rider:false
    }
    Order.getOrders(order, res=>{
    console.log('getorder')
      let orders = res.result.data.data
      console.log('orders',orders)
      that.setData({orders})
      that._classifyOrders(orders)
    })
  },
  
  // 订单分类
  _classifyOrders:function(orders){
    let waitingOrder = []
    let acceptedOrder = []
    let waitGetOrder = []
    let finshedOrder = []
    for(var i in orders){
      switch(orders[i].selfget_status){
        case 0:
          waitingOrder.push(orders[i])
          break;
        case 1:
          acceptedOrder.push(orders[i])
          break;
        case 2:
          waitGetOrder.push(orders[i])
          break;
        case 3:
          finshedOrder.push(orders[i])
          break;
      }
    }
    this.setData({
        waitingOrder: waitingOrder,
        acceptedOrder: acceptedOrder,
        waitGetOrder: waitGetOrder,
        finshedOrder:finshedOrder,
    })
  },
  // 顾客：确认收货
  confirmGet:function(e){
    let orderId = e.detail.orderId
    let order = {order_status:2}
    Order.updateOrder(orderId, order, res=>{
      console.log('更新成功')
      this.onShow()
    })
  },
  // 顾客：删除订单
  delOrder:function(e){
    console.log('delaodre')
    let orderId = e.detail.orderId
    Order.delOrder(orderId, res=>{
      console.log('删除成功')
      this.onShow()
    })
  },
 
  // 订单详情
  orderDetail:function (e) {
    let orderId = e.detail.orderId
    wx.navigateTo({
      url: '/pages/orderDetail/orderDetail?orderId='+orderId,
    })
  },
  //顾客：确认取餐完成  
  confirmGet:function(e){
    console.log('openId',e.detail.orderId)
    let orderId = e.detail.orderId
    let order = {selfget_status:3}
    Order.updateOrder(orderId, order,res=>{
      console.log('更新成功')
      this.onShow()
    })
  },
 // 顾客：再来一单
 anotherOrder:function(e){
  let orderId = e.detail.orderId
  wx.showToast({
    title: '该功能暂未开通!',
    icon:'none'
  })
  },
})
