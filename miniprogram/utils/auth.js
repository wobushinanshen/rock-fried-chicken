
async function checkLogin(){
    return new Promise((resolve, reject) => {
      wx.getSetting({
        success(res) {
            if(res.authSetting['scope.userInfo']){
                return resolve(true)
            }else{
                return resolve(false)
            }
          },
          fail() {
            return resolve(false)
          }
      })
    })
  }
  module.exports = {
    checkLogin:checkLogin
  }