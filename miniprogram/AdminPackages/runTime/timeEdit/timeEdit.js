import { RunTimeModel }  from "../../../models/RunTimeModel.js"
let RunTime = new RunTimeModel()
import Toast from '../../../components/dist/toast/toast';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        runTime:{
            begin_time:'08:00',
            end_time:'12:00',
        }
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if(options){
            this._getRunTime(options.runTimeId)
        }
    },
    _getRunTime:async function(runTimeId){
        let runTime = {
            _id:runTimeId
        }
        let res = await RunTime.getRunTime(runTime)
        this.setData({
            runTime:res[0]
        })
    },
    checkTime:function(begin,end){
        let beginTime = begin.split(':').join('')
        let endTime = end.split(':').join('')
        if(beginTime >= endTime){
          return false
        }else{
            return true
        }
    },
    beginConfirm:function(e){
        let beginTime = e.detail.beginTime
        console.log('begin',endTime)
        this.setData({
            'runTime.begin_time':beginTime
        })
    },
    endConfirm:function(e){
        let endTime = e.detail.endTime
        console.log('endtaime',endTime)
        this.setData({
            'runTime.end_time':endTime
        })
       
    },
    confirm:function(e){
        let that = this
        let runTime = this.data.runTime
        console.log('runtime',runTime)
        let timeId = runTime._id
        delete runTime._id
        let result = this.checkTime(runTime.begin_time, runTime.end_time)
        if(!result){
            Toast.fail('开始时间不能大于结束时间!');
        }else{
           RunTime.updateRunTime(runTime, timeId, res=>{
            Toast({
                type: 'success',
                message: '修改成功',
                onClose: () => {
                  that.cancel()
                },
              });
               
             
           })
        }
    },
    cancel:function(e){
        wx.navigateBack({
          delta: 1,
        })
    },

})