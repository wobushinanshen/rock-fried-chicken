import { RunTimeModel }  from "../../../models/RunTimeModel.js"
let RunTime = new RunTimeModel()
import Toast from '../../../components/dist/toast/toast';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        beginTime:'08:00',
        endTime:'12:00',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
   
    },
    checkTime:function(begin,end){
        let beginTime = begin.split(':').join('')
        let endTime = end.split(':').join('')
        if(beginTime >= endTime){
          return false
        }else{
            return true
        }
    },
    beginConfirm:function(e){
        let beginTime = e.detail.beginTime
        this.setData({beginTime})
    },
    endConfirm:function(e){
        let endTime = e.detail.endTime
        this.setData({endTime})
       
    },
    confirm:async function(e){
        let runTime = {
            begin_time:this.data.beginTime,
            end_time:this.data.endTime
        }
        let result = this.checkTime(runTime.begin_time, runTime.end_time)
        if(!result){
            Toast.fail('开始时间不能大于结束时间!');
        }else{
            let res = await RunTime.createRunTime(runTime)
            Toast.success('添加成功!')
            this.cancel()
        }
    },
    cancel:function(e){
        wx.navigateBack({
          delta: 1,
        })
    },

})