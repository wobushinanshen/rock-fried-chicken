import { RunTimeModel }  from "../../../models/RunTimeModel.js"
let RunTime = new RunTimeModel()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        run_times:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        // this._init()
    },
    onShow:function(){
        this._init()
    },
    _init:function(){
       this._getRunTime()
    },
    _getRunTime:async function(){
        let res = await RunTime.getRunTime({})
        this.setData({
            run_times:res
        })
    },
    addRunTime:function(e){
        wx.navigateTo({
          url: '../timeAdd/timeAdd',
        })
    },
    editTime:function(e){
        let timeId = e.detail.timeId
        wx.navigateTo({
          url: '../timeEdit/timeEdit?timeId='+timeId,
        })
    },
    deleteTime: async function(e){
        wx.showToast({
          title: '暂不可以删除!',
        })

        // let timeId = e.detail.timeId
        // let res = await wx.showModal({
        //     title: '提示',
        //     content: '确认删除该时间点?',
        // })
        // if(res.confirm){
        //     await RunTime.deleteRunTime(timeId)
        // this.onShow
        // }
        
        
    },

   
})