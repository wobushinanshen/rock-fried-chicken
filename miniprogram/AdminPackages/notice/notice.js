// pages/admin/notice/notice.js
import { NoticeModel } from '../../models/NoticeModel.js'
let Notice = new NoticeModel()
import Toast from '../../components/dist/toast/toast'
var app  = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        disabled:true,
        notices:[],
        value: ""

    },
    modifyNotice:function(event){
        console.log('e',event)
        let noticeId = event.currentTarget.dataset.id
        wx.navigateTo({
          url: '../modifyNotice/modifyNotice?noticeId=' +noticeId ,
        })
    },
    _subString:function(array, field, length){
        for(let i=0;i<array.length;i++){
            if(array[i][field].length > length){
                array[i][field] = array[i][field].substr(0,length) + '...'
            }
        }
        return array
    },
    _init:function(){
        this._getNotice()
    },
    _getNotice:async function(){
        let res = await Notice.getNotice({})
        this.setData({
           notices:res 
        })
    },
    submit:async function(e){
        let notice = e.detail.notice
        let noticeId = notice._id
        delete notice._id
        notice.update_time = new Date()
        notice.create_time = new Date()
        console.log('notice',notice)
        await Notice.updateNotice(notice, noticeId)
        this.onShow()
       
    },
    cancel:function(e){
        wx.navigateBack({
          delta: 1,
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },

   
})