// pages/admin/coupon/addCoupon/addCoupon.js
const Asc = require('../../../utils/aes.js')
Page({

    /**
     * 页面的初始数据
     */
    data: {
        subPrice:'',
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let coupon = {
            number : 1,
            begin:'2021-01-15 10:13:20',
            end:'2031-01-15 10:13:20',
            price:22
        }
        let str = JSON.stringify(coupon)
        
        console.log('str',str)
        console.log('en',Asc.aesEncrypt(str))
        console.log('de',JSON.parse(Asc.aesDecrypt(Asc.aesEncrypt(str))))
    },
    inputSubprice:function(e){
        this.setData({
            subPrice:parseInt(e.detail)
        })
    },

   
})