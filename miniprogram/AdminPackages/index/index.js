// pages/admin/index/index.js
import { NoticeModel } from '../../models/NoticeModel.js'
import Toast from '../../components/dist/toast/toast'
import { OrderModel } from '../../models/OrdelModel.js'
// const OrderService = require('../../service/OrderService.js')
let Notice = new NoticeModel()
let Order = new OrderModel()
var app = getApp()

Page({


    /**
     * 页面的初始数据
     */
    data: {
        notices:[],
        value:'',
        active: 'home',
        bc:`--bc:yellow;`
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
      
    },
    _init:async function(){
      //公告
      // Notice.getNotice(res=>{
      //   this.setData({
      //       notices:res.result.data.data,
      //   })
      // })
      let order = {
        order_status:0
      }
      let result =await Order.getOrder(order)
      // let result2 = await OrderService.getOrder(order)
      console.log('result',result)
      // console.log('reslut2',result2)
    },
    // 订单
    toOrders:function(){
      wx.navigateTo({
        url: '../orders/index/index',
      })
    },
    toShop:function(){
      wx.navigateTo({
        url: '../shop/addShop/addShop',
      })
    },
    toProduct:function(){
      wx.navigateTo({
        url: '../product/index/index',
      })
    },
    toCustomer:function(){
      wx.showToast({
        title: '即将开通该功能!',
        icon:'none'
      })
    },
    // 公告
    toNotice:function(){
      wx.navigateTo({
        url: '../notice/notice',
      })
    },
    // VIP设置
    toVip:function(){
      wx.navigateTo({
        url: '../vip/index/index',
      })
    },
    //打印机
    toPrinter:function(){
      wx.navigateTo({
        url: '../printer/index/index',
      })
    },
    toCoupon:function(){
      wx.showToast({
        title: '即将开通该功能!',
        icon:'none'
      })
    },
    // 设置 密码
    toSetting:function(){
      wx.navigateTo({
        url: '../modifyPwd/modifyPwd',
      })
    },
    // 主页
    toHome:function(){
      wx.switchTab({
        url: '../../pages/index/index',
      })
    },

   
  
})