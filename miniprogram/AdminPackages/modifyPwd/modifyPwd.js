import { ShopModel } from "../../models/ShopModel.js"
let Shop = new ShopModel()
import Toast from '../../components/dist/toast/toast';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        password:'',
        oldPassword : '',
        newPassword : '',
        confirmPassword : '',
        disabled : true ,
        admin : {}

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:async function(){
        let res = await Shop.getShop({})
        this.setData({
            admin:res[0]
        })
       
    },

    //  旧密码
    inputOldPwd:function(e){
        let oldPassword = e.detail
        if(oldPassword){
            this.setData({
                oldPwdError : '',
                oldPassword : oldPassword
            })
        }else{
            this.setData({
                oldPwdError : '旧密码不能为空'
            })
        }
        this.checkInput()
    },
    //新密码    
    inputNewPwd:function(e){
        let newPassword = e.detail
        if(newPassword){
            this.setData({
                newPwdError : '',
                newPassword : newPassword
            })
        }else{
            this.setData({
                newPwdError : '新密码不能为空'
            })
        }
        this.checkInput()
    },
    
    // 确认密码
    inputConfirmPwd:function(e){
        let confirmPassword = e.detail
        if(confirmPassword){
            this.setData({
                confirmPwdError : '',
                confirmPassword : confirmPassword
            })
        }else{
            this.setData({
                confirmPwdError : '确认密码不能为空'
            })
        }
        this.checkInput()
    },
    // 输入不为空
    checkInput:function(){
        if(this.data.oldPassword.length > 0 && this.data.newPassword.length > 0 && this.data.confirmPassword.length > 0){
            this.setData({
                disabled : false
            })
        }else{
            this.setData({
                disabled : true
            })
        }
    },
    _checkEmpty:function(str){
        if(str == '' || str == null){
            return true
        }else{
            return false
        }
    },
    _checkSubmit:function(){
        if(this._checkEmpty(this.data.oldPassword)){
            return '旧密码不能为空!'
        }
        if(this._checkEmpty(this.data.newPassword)){
            return '新密码不能为空!'
        }
        if(this._checkEmpty(this.data.confirmPassword)){
            return '密码不能为空!'
        }
        if(this.data.newPassword !== this.data.confirmPassword){
            return '两次密码不一致!'
        }
        if(this.data.oldPassword.trim() !== this.data.admin.password.trim()){
            return '旧密码错误!'
        }
        return ''

        
    },
    confirm:async function(){
       let message = this._checkSubmit()
       if(message.length > 0){
           wx.showToast({
             title: message,
             icon:'none'
           })
       }else{
        let admin = {
            password : this.data.newPassword
        }
        await Shop.updateShop(admin, this.data.admin._id)
        Toast.success('密码修改成功')
            this.cancel()
       }   
    },
    cancel:function(){
        wx.navigateBack({
          delta: 1,
        })
    }
    
})