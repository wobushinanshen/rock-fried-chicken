const Print = require('../../../utils/print.js')
import { PrinterModel } from '../../../models/PrinterModel.js'
let  Printer = new PrinterModel()
Page({
    data:{
        machineCode:'',
        msign:'',
        clientId:'',
        printer:{},
        status:'离线'//打印机状态
        
    },
    onLoad:function(){
      this._init()
    },
    async _init(){
      await this._getPrinter()
      await this.getState()
    },
    // 获取打印机信息
    _getPrinter:async function(){
      let res = await Printer.getPrinter()
      this.setData({
        printer:res[0]
      })
    },
    // 扫码添加
    scanAdd:function(e){
        let that = this
        // 检查是否授权相机权限
        that._setAuthorize()
        // 调用相机扫描二维码，识别打印机终端号和密钥
        wx.scanCode({
          onlyFromCamera: true,
          success (res) {
              let result = JSON.parse(res.result)
              console.log('result',result)
              result.client_id = that.data.printer.client_id
              let machineCode = result.machineCode
              let msign = result.msign
              that.setData({
                machineCode:machineCode,
                msign:msign
              })
              that._addPrinter(machineCode, msign)
             
          },fail(res){
              wx.showToast({
                title: '未识别到有效信息，请更换图片重试',
                icon:'none',
                duration: 2000
              })
          }
        })
        console.log('scan',e)
    },
    // 添加打印机
    _addPrinter: async function(machineCode, msign){
      let printer = {
        machine_code:machineCode,
        msign: msign
      }
      //   检查缓存中是否已有 access_token
      var token = wx.getStorageSync('access_token');
        // 1、如果已经有（不是第一次执行scanAdd()函数）
      if(token){
        // 直接添加打印机
        Print.addPrinter(machineCode, msign)
      }else{
        //  2、如果没有access_token，则先获取再添加打印机
        Print.getToken()
        Print.addPrinter(machineCode, msign)
      }
      await Printer.updatePrinter(printer, this.data.printer._i)
    },
    // 手动添加打印机
    handAdd:function(e){
      wx.navigateTo({
        url: '../addPrinter/addPrinter',
      })
    },
    // 设置声音大小
    setSound:function(e){
      wx.navigateTo({
        url: '../setSound/setSound',
      })
    },
    _setAuthorize:function(){
    // 获取用户授权设置，如果用户第一次进入未授权会出现弹窗
    wx.getSetting({
        success: response => {
            console.log('success',response)
          if (!response.authSetting['scope.camera']) {
            wx.authorize({
              scope: 'scope.camera',
              success(res) {
                // 同意摄像头
                console.log('授权成功')
              },
              fail(res) {
                // 不同意摄像头
                console.log('授权失败')
              }
            })
          }
        },
        fail: res => {

        },
        complete: res => {
        },
      }) 
    },
    // 打印示例
    print:function(){

    //小票排版
    var strA = '.';
    var strB = '*';
    var content = "<FS2><center>**#1 微信小程序**</center></FS2>";
    content += strA.repeat( 32);
    content += "<FS2><center>--在线支付--</center></FS2>";
    content += "<FS><center>摇滚炒鸡</center></FS>";
    content += "订单时间:2018-12-27 16:23\n";
    content += "订单编号:40807050607030\n";
    content += strB.repeat( 14) + "商品" + strB.repeat( 14);
    content += "<table>";
    content += "<tr><td>烤土豆(超级辣)</td><td>x3</td><td>5.96</td></tr>";
    content += "<tr><td>烤豆干(超级辣)</td><td>x2</td><td>3.88</td></tr>";
    content += "<tr><td>烤鸡翅(超级辣)</td><td>x3</td><td>17.96</td></tr>";
    content += "<tr><td>烤排骨(香辣)</td><td>x3</td><td>12.44</td></tr>";
    content += "<tr><td>烤韭菜(超级辣)</td><td>x3</td><td>8.96</td></tr>";
    content += "</table>";
    content += strA.repeat( 32);
    content += "<QR>这是二维码内容</QR>";
    content += "小计:￥82\n";
    content += "折扣:￥４ \n";
    content += strB.repeat(32);
    content += "订单总价:￥78 \n";
    content += "<FS2><center>**#1 完**</center></FS2>";

    let machineCode = this.data.printer.machine_code
    console.log('machineCode',machineCode)
    let orderId = '2020'+new Date().getTime()
    Print.toPrint(machineCode,orderId,content)
    },
    //打印机状态
    getState:function(){
      let machineCode = this.data.printer.machine_code
      Print.getState(machineCode)
    },
    // 取消所有未打印订单
    cancelOrders:function(){
      let machineCode = this.data.printer.machine_code
      Print.cancelOrders(machineCode)
    },
    // 设置默认自动接单打印
    defaultPrint:async function(){
      let printer = {
        default_print : !this.data.printer.default_print
      }
      await Printer.updatePrinter(printer, this.data.printer._id)
    }
    

    
})