const Print = require('../../../utils/print.js')
import { PrinterModel } from '../../../models/PrinterModel.js'
let  Printer = new PrinterModel()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        machineCode:'',
        clientSecret:'',
        printer:{}

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:async function(){
        let res = await Printer.getPrinter()
        this.setData({
          printer:res[0]
        })
    },
    inputMachineCode:function(e){
       let machineCode = e.detail
       this.setData({machineCode})

 
    },
    inputClientSecret:function(e){
        let clientSecret = e.detail
        this.setData({clientSecret})

       
    },
    cancel:function(){
        wx.navigateBack({
          delta: 1,
        })
    },
    submit:async function(){
        let printerId = this.data.printer._id
        let printer = {
            client_id:this.data.clientSecret,
            machine_code:this.data.machineCode
        }
        let message = this._checkSubmit()
        if(message.length > 0){
            wx.showToast({
              title: message,
              icon:'none'
            })
        }else {
            this._addPrinter(printer.machine_code, printer.client_id)
            await Printer.updatePrinter(printer, printerId)
            this.cancel()
           
        }
         
    },
    _checkSubmit:function(){
        let clientSecret = this.data.clientSecret
        let machineCode = this.data.machineCode
        if(clientSecret.length <= 0){
            return '终端号不能为空!'
        }
        if(machineCode.length <= 0){
            return '密钥不能为空!'
        }
        return ''
    },
    _addPrinter:async function(machineCode, msign){
        let printer = {
          machine_code:machineCode,
          msign: msign
        }
        //   检查缓存中是否已有 access_token
        var token = wx.getStorageSync('access_token');
          // 1、如果已经有（不是第一次执行scanAdd()函数）
        if(token){
          // 直接添加打印机
          Print.addPrinter(machineCode, msign)
        }else{
          //  2、如果没有access_token，则先获取再添加打印机
          await  Print.getToken()
          await Print.addPrinter(machineCode, msign)
        }
        await Printer.updatePrinter(printer, this.data.printer._id)
       
      },


   
})