import { BannerModel }  from "../../models/BannerModel.js"
let Banner = new BannerModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        banners:[],
        delImages:[],
        BannerImages:[],
    },
    showDetail:function(e){
    
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
       Banner.getBanner(res=>{
            
            let banners = res.result.data.data
            this.setData({banners})
       })
    },
    deleteBanner:function(event){
        let banner = event.detail.banner
        let that = this
        wx.showLoading({
            title: '图片删除中',
          })
        let imagePath = banner.image.replace("cloud://shop-cloud-test-zpc4s.7368-shop-cloud-test-zpc4s-1302621149","")
        console.log(imagePath)
        Banner.delBanner(banner._id, res=>{
            console.log('删除数据成功',res)
            that._delImage([banner.image])
            that._init()
            wx.hideLoading({
              complete: (res) => {},
            })
            that.onLoad()
        })
    },
    _delImage:function(delImgs){
        wx.cloud.deleteFile({
          fileList:delImgs,
          success:res=>{
            console.log('删除成功', res)
          }
        })
      },

    // 上传图片
    _uploadImages:function(images,directory){
    images.forEach(element=>{
        let imagepath = element
        let name = imagepath.substr(imagepath.length-20,imagepath.length)
        wx.cloud.uploadFile({
        // 指定上传到的云路径
        cloudPath: directory+'/'+name,
        // 指定要上传的文件的小程序临时文件路径
        filePath: imagepath,
        // 成功回调
        success: res => {
            console.log('上传成功', res)
        },
        })
    })
    },
    //选择图片
    addBanner() {
        var that = this;
        wx.chooseImage({
        count: 1, //修改上传图片最大数量
        sizeType: ["original", "compressed"], //可以指定是原图还是压缩图，默认二者都有
        sourceType: ["album", "camera"], //从相册选择
        success: (res) => {
            if (res.tempFiles[0] && res.tempFiles[0].size > 1024 * 1024) {
            wx.showToast({
                title: "图片不能大于1M",
                icon: "none"
            })
            return;
            }
            //校验图片
            var filePath = res.tempFilePaths[0];
             that.ImgCheck(filePath)
             
        }
        });
    },
    //校验图片
    ImgCheck:function(filePath){
        wx.showLoading({
          title: '图片加载中',
        })
         // 图片转化buffer后，调用云函数
         let that = this
         wx.getFileSystemManager().readFile({
           filePath: filePath,  
           success: res => {
               wx.cloud.callFunction({
                name: "ContentCheck",
                data: {
                  $url:'ImgCheck',
                  ImgBuffer: res.data,
                }
              }).then(
                imgRes => { 
                  if (imgRes.result.data.errCode == "87014") {
                    wx.hideLoading({
                      complete: (res) => {},
                    })
                    wx.showToast({
                      title: "图片含有违法违规内容",
                      icon: "none"
                    })
                  } else {
                    //图片正常 
                    console.log("图片郑")
                    wx.hideLoading({
                      complete: (res) => {},
                    })
                    // 上传图片
                    that._uploadImages([filePath],'banner')
                    // 更新本页面数据
                    if (that.data.banners.length == 0) {
                        that.setData({
                            banners: [{image:filePath}],
                        })
                      } else {   
                        that.setData({
                            banners: that.data.banners.concat({image:filePath})
                        })
                     } 
                   //上传数据到数据库
                   let image  ='/banner/' + filePath.substr(filePath.length-20,filePath.length)
                  
                   let banner = {image:image} 
                   banner.create_time = new Date().toLocaleString()
                   Banner.addBanner(banner, res=>{
                       console.log('上传数据成功',res)
                       that.onLoad()
            
                   })
                  } 
                },
                function (error) {
                  wx.hideLoading({
                    complete: (res) => {},
                  })
                  wx.showToast({
                    title: "失败，请重试",
                    icon: "none"
                  })
                  console.log('error',error)
             })
        } 
       }) 
      },

    
})