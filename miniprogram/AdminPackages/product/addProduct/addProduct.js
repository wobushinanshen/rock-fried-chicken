// pages/admin/addproduct/addproduct.js
import { ProductModel }  from "../../../models/ProductModel.js"
import { CategoryModel } from "../../../models/CategoryModel.js"
import Toast from '../../../components/dist/toast/toast'
let Product = new ProductModel()
let Category = new CategoryModel()
Page({
      /**
     * 页面的初始数据
     */
    data: {
      categories: [],
      productImages:[],//商品图片
      product:{},
      productError:{},
    },
  onLoad: function (options) {
    this._init()
  },
  _init:async function(){
    // 加载分类
    let res = await Category.getCategory({})
    // 格式化数据结构
    var newArr = res.map(function(item){
      return{
        text:item['category_name'],
        value:item['category_type']
      }
    })
    this.setData({
      categories: newArr
    })
   
  },
    

    _spliceString:function(arrays,length,directory, name){
      let newarr = []
      console.log('array',arrays)
      arrays.forEach(element=>{
      let str = element.url.substr(element.url.length-length,element.url.length)
        newarr.push('/'+directory+'/'+ name + str)
        console.log('for',str)
      })
      return newarr
    },
    /**
     * 上传文件到云环境的存储空间
     * @param {上传到的云路径} cloudPath 
     * @param {文件的临时路径} filePath 
     */
    _uploadImages:function(cloudPath,filePath){
      //检查文件是否存在
      if(filePath===""){
        console.log('filepath is not exist')
        //不存在
      }else{
        wx.cloud.uploadFile({
          // 指定上传到的云路径
          cloudPath: cloudPath,
          // 指定要上传的文件的小程序临时文件路径
          filePath: filePath,
          // 成功回调
          success: res => {
            console.log('上传成功', res)
          },
        })
      }
    },
    
    //校验价格格式
    checkPrice:function(input){
     var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
     return reg.test(input)
    },
    // 校验数字
    checkNumber:function(input){
      var reg = /^[1-9]\d*$/;
      return reg.test(input)
    },
    // 校验内容全部填写且没有错误，'添加商品' 按钮可以点击
    checkInput:function(){
      if(this.data.productError.name == '' && this.data.productError.description == '' && this.data.productError.price == '' && this.data.productError.sellPrice == '' && this.data.productImages.length > 0){
        this.setData({
          showButton:false
        })
      }else{
        this.setData({
          showButton:true
        })
      }
    },
    // 提交前校验商品填写信息
   checkSubmit: function(e){
    if(e.product_name == ''){
      return '商品名称不能为空!'
    }
    if(e.product_price == ''){
      return '市场价不能为空!'
    }
    if(!this.checkPrice(e.product_price)){
      return '市场价必须为数字!'
    }
    if(e.product_sell_price == ''){
      return '促销价不能为空!'
    }
    if(!this.checkPrice(e.product_sell_price)){
      return '促销价必须为数字!'
    }
    if(this.data.productImages.length == 0){
      return '未选择商品图片!'
    }
    return ''
  },
    // 删除图片：图片上传完成后，点击右上角叉号可以删除
    deleteImage:function(event){
      let productImages = this.data.productImages
      productImages.splice(0,1)
      this.setData({productImages})
      this.checkInput()
    },

    //选择商品图片（单个）
    async afterRead(event) {
      let imagePath = event.detail.file.path
      wx.showLoading({
        title: '图片加载中!',
      })
      // 调用校验方法
      let res = await this.CheckImage(imagePath)
      console.log('res',res)
      wx.hideLoading({
        success: (res) => {},
      })
      // 如果校验失败
      if(!res){
        Toast.fail('图片上传失败!')
      //  校验成功 
      }else{
        Toast('图片上传成功!')
        this.setData({
          productImages:[{name:'image',url:imagePath}]
        })

      }
    },
    // 校验文字
    CheckMsg:async function(msg){ 
     let res =  await wx.cloud.callFunction({
        name:'ContentCheck',
        data: {
          $url:'MsgCheck',
          msg:msg
        }
      })
      if(res.result.data.errCode == '87014'){
        return false
      }else{
        return true
      }
    },
  
  uploadPicture:function(e){
    console.log('e',e)
    let fileList = this.data.fileList
    let filePath = e.detail.file.path
    if(filePath){
        fileList.push(filePath)
    }
    this.CheckImage(filePath)

},
 
  /**
   * 上传文件到云环境的存储空间
   * @param {上传到的云路径} cloudPath 
   * @param {文件的临时路径} filePath 
   */
  _uploadImages:function(cloudPath,filePath){
      //检查文件是否存在
      if(filePath===""){
        console.log('filepath is not exist')
        //不存在
      }else{
        wx.cloud.uploadFile({
          // 指定上传到的云路径
          cloudPath: cloudPath,
          // 指定要上传的文件的小程序临时文件路径
          filePath: filePath,
          // 成功回调
          success: res => {
            console.log('上传成功', res)
          },
        })
      }
    },
    /**
  * 
  * @param {*} filePath  图片临时存储地址
  */
  CheckImage:async function(filePath){
 
  // 图片转化buffer后，调用云函数
  let arrayBuffer =  wx.getFileSystemManager().readFileSync(filePath)
  console.log('res',arrayBuffer)
  let checkResult = await wx.cloud.callFunction({
                  name: "ContentCheck",
                  data: {
                    $url:'ImgCheck',
                    ImgBuffer: arrayBuffer,
                  }
  })
  if(checkResult.result.data.errCode == 87014){
      // wx.hideLoading({
      //   success: (res) => {},
      // })
      console.log('内容违法')
      return false
  }else{
      console.log('校验成功')
      return true
      
  }
  },
  _handlerSubmit:async function(evt){
    console.log('evt',evt)
    let product = evt.detail.value
    let message = this.checkSubmit(product)
    if(message.length > 0){
      wx.showToast({
        title: message,
        icon:'none'
      })
      return
    }
    product.product_price = parseFloat(product.product_price)
    product.product_sell_price = parseFloat(product.product_sell_price)
    product.product_image = this._spliceString(this.data.productImages,10,"product",product.product_name)[0]
    product.product_status = 1 
  
    //1、上传图片
    this._uploadImages(product.product_image.slice(1),this.data.productImages[0].url )

    // 2、上传商品信息到数据库
    await Product.createProduct(product)
    // 3、商品添加成功提示
    Toast.success('商品添加成功')
    wx.navigateBack({
      delta: 1,
    })
    
  },
  
})