// AdminPackages/product/index/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    toAddProduct:function(){
        wx.navigateTo({
          url: '../addProduct/addProduct',
        })
    },
    toProductList:function(){
        wx.navigateTo({
          url: '../productList/productList',
        })
    },
    toModifyProduct:function(){
      wx.navigateTo({
        url: '../modifyProduct/modifyProduct',
      })
    },
    toCategory:function(){
      wx.navigateTo({
        url: '../../category/categoryList/categoryList',
      })
    }


    
})