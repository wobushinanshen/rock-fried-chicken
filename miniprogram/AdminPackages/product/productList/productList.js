// pages/admin/productList/productList.js
import { ProductModel }  from "../../../models/ProductModel.js"
let Product = new ProductModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        products: []

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    onShow:function(){
        this._init()
    },
    _init:function(){
        this._getProduct()
    },
    _getProduct:async function(){
        
        let res = await Product.getProduct({})
        this.setData({
          products:res,
        })
      },
        // 跳转商品详情
    productDetails: function (e) {
        
        this._navProductDetail(e.detail.productId)
    },
    productBanner: function (event) {
        let product_id = Product.getDataSet(event, "productid")
        this._navProductDetail(product_id)
        
    },
    // 跳转详情
    _navProductDetail: function (product_id) {
        wx.navigateTo({
        url: '../modifyproduct/modifyproduct?product_id=' + product_id,
        })
    },
    modifyProduct:function(e){
        let productId = e.detail.productId
        wx.navigateTo({
          url: '../modifyProduct/modifyProduct?productId=' + productId,
        })
    },
    // 删除云端图片
    _delCloudImage:function(imagePath){
    wx.cloud.deleteFile({
        fileList:[imagePath],
        success:res=>{
        console.log('删除成功', res)
        }
    })
    },
    deleteProduct:async function(e){
        let res = await wx.showModal({
            title: '提示',
            content: '确定删除该商品?',
          })
          console.log('modal',res)
        if(res.confirm){
            let product = e.detail.product
            // 1、删除商品图片
            await this._delCloudImage(product.product_image)
            // 2、删除商品数据
            await Product.deleteProduct(product._id)
            wx.showToast({
            title: '删除成功!',
            })
            this.onShow()
        }
        
    }

   
})