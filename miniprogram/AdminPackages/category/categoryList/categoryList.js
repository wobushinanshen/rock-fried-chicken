// pages/admin/Category/Category.js
import { CategoryModel } from "../../../models/CategoryModel.js"
import Toast from '../../../components/dist/toast/toast';

let Category = new CategoryModel()
Page({
   /**
     * 页面的初始数据
     */
    data: {
      categories:[],
      show:false,
      input:"",
      showConfirm:false
  },
  /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      this._init()
  },
  _init:function(){
      this._getCategory()
  },
   
    // 1 删除商品分类
    categoryDetails:function(e){
        Category.delCategory(e.detail.categoryId,res=>{
            console.log('删除商品分类成功',res)
        })
        Toast.success('删除成功');
        wx.redirectTo({
          url: '../Category/Category',
        })

    },
   _getCategory:async function(){
        let res = await Category.getCategory({})
        this.setData({
          categories:res
        })
    },
   _deletCategory:async function(){
     let categoryId = ''
     let res = await Category.deleteCategory(categoryId)
     await wx.showToast({
       title: '删除成功!',
       icon:'none'
     })
     this.onShow()
    },
   _modifyCategory:async function(){

    },
    //显示弹窗
    _addCategory:function(event){
        this.setData({
            show:true
        })
    },
    //点击确定 将输入数据添加到数据库
    confirm:function(e){
        let category = {
            category_name: this.data.input,
            category_type: this.data.categories.length+1,
            create_time: Mydate.getDate(),
            update_time: Mydate.getDate()
        }
        console.log('category',category)
        Category.addCategory(category,res=>{
            console.log('添加成功',res)
        })
        Toast.success('添加成功');
        wx.redirectTo({
            url: '../Category/Category',
          })
      },
      _add:function(){
          this.setData({
            show:true
          })
      },
      onChange(event) {
        if(event.detail.length>0){
          this.setData({
            showConfirm:true
          })
        }else{
          this.setData({
            showConfirm:false
          })
        }
        this.setData({
          input:event.detail
        })
      },
      onClose() {
        this.setData({ close: false });
      },

   

    

    
})