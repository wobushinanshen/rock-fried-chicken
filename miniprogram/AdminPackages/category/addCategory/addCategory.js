// pages/admin/addCategory/addCategory.js
import { CategoryModel } from "../../../models/CategoryModel.js"
import { ProductModel }  from "../../../models/ProductModel.js"


let Category = new CategoryModel()

let Product = new ProductModel()
Page({
    onChange(event) {
        if(event.detail.length>0){
            this.setData({
                disabled:false,
                categoryName:event.detail
            })
        }else{
            this.setData({
                disabled:true
            })
        }
      },
      _cancle:function(){
        wx.navigateBack({
            delta: 1,
        })
      },
      _addCategory:function(){
        let category = {
            category_name: this.data.categoryName,
            category_type: this.data.categories.length+1,
            create_time: Mydate.getDate(),
            update_time: Mydate.getDate()
        }
        console.log('category',category)
        Category.addCategory(category,res=>{
            wx.showToast({
                title: '添加分类成功',
                duration:1000
              }) 
        })
        wx.navigateTo({
            url: '../index/index',
          })
        
      },

    /**
     * 页面的初始数据
     */
    data: {
        categoryName:"",
        disabled:true,
        categories:[],
        modifyShow:false,//弹出修改弹窗
        addShow:false,//弹出添加弹窗
        category:{},

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        Category.getCategory(res=>{
            let categories = res.result.data.data
            this.setData({categories})
        })
        Product.getProduct(res=>{
            console.log('pres',res.result.data.data)
        })
    },
    // 1、编辑分类
    editCategory:function(e){
        let category = e.detail
        
        this.setData({
            modifyShow:true,
            category:category
        })
        console.log('e',this.data.category)
    },
    editInput:function(e){
        let category_name = e.detail
        this.setData({
            'category.category_name':category_name
        })
    },
    confirmModify:function(){
        let category = this.data.category
        let categoryId = this.data.category.category_id
        delete category.category_id
        Category.updateCategory(category, categoryId, res=>{
            console.log('update',res)
            this.onLoad()
        })
    },
    // 2、新建分类

    addCategory:function(e){
        this.setData({
            addShow:true,
        })
       
    },
    addInput:function(e){
        let category_name = e.detail
        this.setData({
            'category.category_name':category_name
        })
    },
    confirmAdd:function(){
        let category = this.data.category
        let categories = this.data.categories
        let max = 0
        if(categories.length>0){
            max = categories[0].category_type
            categories.forEach(item => max = item.category_type > max ? item.category_type : max)
        }
        category.category_type = max + 1
        Category.addCategory(category, res=>{
            this.onLoad()
        })
    },
    // 3、删除分类

    deleteCategory:function(e){
        let category_id = e.detail.category_id
        Category.delCategory(category_id, res=>{
            console.log('delete',res)
            this.onLoad()
        })
        console.log('del',e)
    }



})