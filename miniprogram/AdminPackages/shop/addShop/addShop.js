// pages/test/test.js
import { ShopModel } from "../../../models/ShopModel.js"
import Toast from '../../../components/dist/toast/toast';
const key = 'Y2LBZ-LQ26W-NALRX-OKNQS-HYALF-S4F4G'; //使用在腾讯位置服务申请的key
const referer = '摇滚炒鸡-顾客端'; //调用插件的app的名称
// 引入SDK核心类
var QQMapWX = require('../../../utils/qqmap-wx-jssdk.js');
var qqmapsdk = new QQMapWX({
    key: key // 必填
});
let Shop = new ShopModel()
const chooseLocation = requirePlugin('chooseLocation');

Page({

    /**
     * 页面的初始数据
     */
    data: {
        position:{},
        location: {
            latitude: 39.984060,
            longitude: 116.307520
          },
        address:'',//营业地址
        detailAddress:'',//详细地址
        region: ['广东省', '广州市', '海珠区'],//picker默认选中
        customItem: '全部',//
        beginShow:false,
        endShow:false,
        show:false,
        beginTime:'09:00',
        endTime:'12:00',
        Error:{},
        shop:{},
        switch:true,
    },
     /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
        this._init()
    },
    onShow () {
        const location = chooseLocation.getLocation(); // 如果点击确认选点按钮，则返回选点结果对象，否则返回null
        if(location){
          this.setData({
            detailAddress:location.name
          })
        }
      },
    changeSwitch:function(e){
        this.setData({
            switch:e.detail
        })
        console.log('switch',e.detail)
    },
    onUnload () {
    // 页面卸载时设置插件选点数据为null，防止再次进入页面，geLocation返回的是上次选点结果
      chooseLocation.setLocation(null);
    },
    // 腾讯地图选点插件 
    choiceDetail:function(e){
         wx.navigateTo({
             url: 'plugin://chooseLocation/index?key=' + key + '&referer=' + referer,
         });
    },
   
    _init:async function(){

        let res = await Shop.getShop({})
        // 如果不是第一次登陆
        console.log('shop',res)
        if(res[0].shop_region.length>0){
            this.setData({
                region:res[0].shop_region,
                switch:res[0].shop_status,
                'Error.shopRegion':'',
                shop:res[0],
                detailAddress:res[0].detail_address
                
            })  
            return
        }
        // 是第一次登录
        let that = this
        wx.getLocation({
        altitude: 'true',//使用高精度定位
        type:'gcj02',//采用gcj02坐标系
        isHighAccuracy:'true',
        success:res=>{
            let location = {}
            location.latitude = res.latitude
            location.longitude = res.longitude
            that.setData({location})
            // 由坐标到坐标所在位置的文字描述的转换  
            qqmapsdk.reverseGeocoder({
                location:location,
                success:res=>{
                    let address = res.result.formatted_addresses.recommend
                    let detail = res.result.address_component
                    let region = []
                    region.push(detail.province)
                    region.push(detail.city)
                    region.push(detail.district)
                    that.setData({
                    region:region
                    })
                    that.setData({
                        detailAddress:address
                    })
                    console.log('success',res)
                }
            })
        }
        })
    },
    // 校验手机号
    isPhoneAvailable:function(poneInput) {
        var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
        if (!myreg.test(poneInput)) {
            return false;
        } else {
            return true;
        }
    },
    // 校验数字
    isNumber:function(number){
        var reg = /^[0-9]*$/
        if(!reg.test(number)){
            return false
        }else{
            return true
        }
    },
    // 输入商家名称
    inputShopName:function(e){
        let name = e.detail
        if(name.length == ''){
            this.setData({
                'Error.shopName':'输入不能为空!'
            })
        }else{
            this.setData({
                'Error.shopName':''
            })
            if(name.length > 20){
                this.setData({
                    'Error.shopName':'商家名称长度不能大于20!'
                })
            }else{
                this.setData({
                    'Error.shopName':''
                })
                this.CheckMsg(name,'shopName')
            }
        } 

    },
    // 输入商家手机号
    inputShopPhone:function(e){
        let phone = e.detail
        if(phone.length == ''){
            this.setData({
                'Error.shopPhone':'输入不能为空!'
            })
        }else{
            this.setData({
                'Error.shopPhone':''
            })
            if(!this.isPhoneAvailable(phone)){
                this.setData({
                    'Error.shopPhone':'手机号码格式错误!'
                })
            }else{
                this.setData({
                    'Error.shopPhone':''
                })
                this.CheckMsg(phone,'shopPhone') 
               
            }
               
        } 
     
    },
    // 输入外卖配送半径
    inputShopRadius:function(e){
        let radius = e.detail
        if(radius.length == ''){
            this.setData({
                'Error.shopRadius':'输入不能为空!'
            })
        }else{
            this.setData({
                'Error.shopRadius':''
            })
            if(!this.isNumber(radius)){
                this.setData({
                    'Error.shopRadius':'只能输入数字!'
                })
            }else{
                this.setData({
                    'Error.shopRadius':''
                })
                if(radius>100){
                    this.setData({
                        'Error.shopRadius':'输入不能大于100!'
                    })
                }else{
                    this.setData({
                        'Error.shopRadius':''
                    })
                }

            }

        }
    },
    checkEmpty:function(str){
        if(str == '' || str == null){
            return true
        }else{
            return false
        }
    },
    checkInput:function(){
        let Error1 = this.data.Error
        console.log('error',Error1)
         if(!(JSON.stringify(Error1) == "{}")){
             if(this.checkEmpty(Error1.shopName) && this.checkEmpty(Error1.shopPhone) && this.checkEmpty(Error1.shopRadius) && this.checkEmpty(Error1.shopRegion)){
                console.log('true')
                return true
             }else{
                console.log('fase')
                 return false
             }
         }else{
            return false
         }
         
    },
    bindRegionChange: function (e) {
        this.setData({
          region: e.detail.value
        })
      },
    
   CheckMsg:function(msg,attribute){ 
       attribute = 'Error.' + attribute
       wx.cloud.callFunction({
        name:'ContentCheck',
        data: {
          $url:'MsgCheck',
          msg:msg
        }
      }).then(MsgRes=>{
        if (MsgRes.result.data.errCode == "87014") {
            this.setData({
                [attribute]:'输入含有非法内容'
            })
        }else{
            this.setData({
                [attribute]:''
            })
        }
      })
    },

    submit:function(e){
        console.log('e',e.detail.value)
        let result = e.detail.value
        result.shop_radius = parseInt(result.shop_radius)
        result.shop_region = result.shop_region.split(' ')
        result.shop_location = this.data.location
        let shop = this.data.shop
       
        if(this.checkInput()){
            Shop.updateShop(result, shop._id, res=>{
                console.log('修改成功')
                wx.navigateBack({
                  delta: 1,
                })
            })
        }else{
            Toast.fail('输入内容不合法,请检查');
        }
      },

})
//在Page({})中使用下列代码
