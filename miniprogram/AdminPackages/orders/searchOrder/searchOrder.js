// pages/admin/searchOrder/searchOrder.js
import { OrderModel } from '../../../models/OrdelModel.js'
let Order = new OrderModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        orders:[],
        identity:true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        console.log(options)
        if(options.orderId){
            let order = {
                order_id:options.orderId
            }
            this._getOrder(order)
        }
    },
    _getOrder:function(order){
        Order.getOrders(order, res=>{
            let orders = res.result.data.data
            this.setData({orders})
        })
    },
     // 订单详情
    orderDetail:function (e) {
    let orderId = e.detail.orderId
    wx.navigateTo({
      url: '/pages/orderDetail/orderDetail?orderId='+orderId,
    })
  },

  
})