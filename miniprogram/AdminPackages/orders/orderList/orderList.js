// pages/my/my.js
const Print = require('../../../utils/print.js')
// const OrderService  = require('../../../service/OrderService.js')
import { PrinterModel } from '../../../models/PrinterModel.js'
import { OrderModel } from '../../../models/OrdelModel.js'
let  Printer = new PrinterModel()
let Order = new OrderModel()
var App = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    orders: [],
    doingOrder:[],
    selfGetOrder:[],
    takeOutOrder:[],
    completedOrder:[],
    openId: "",
    order:{},
    printer:{},
    identity:false,
    
  },
  // 初始化
  _init: function () {
    this._getOrders()
    this._getPrinter()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onLoad:function (options) {
    this._init()
    this._watchOrder()
  },
  onShow:function(){
    this._init()
  },
  // 获取 所有进行中的订单
  _getOrders:async function(){
    let order = {

    }
    let result = await Order.getOrder(order)
    console.log('order',result)
    if(result.length > 0){
      this.setData({
        orders:result
      })
      this._classifyOrders(result)
    }
  },
  _watchOrder:function(){
    let that = this
    let db = wx.cloud.database()
    db.collection('order').where({
      order_status:0
    }).watch({
      onChange: function (snapshot) {
        //监控数据发生变化时触发
        let newOrders = snapshot.docs
        console.log('snap',snapshot)
        that.onShow()
        // if(newOrders.length > 0){
        //   // 方式1：新订单下单直接打印
        //   if(that.data.printer.default_print){
        //     console.log('直接打印')
        //     that.onShow()
        //     Printer.getPrinter(res=>{
        //       let machineCode = res.result.data.data[0].machine_code
        //       that._printNewOrders(newOrders,machineCode)
        //     })
        //   }else{
        //     that.onShow()
        //   }
        // }
      },
      onError:(err) => {
        console.error(err)
      }
    })
  },
  // 打印新订单
  _printNewOrders:function(orders, machineCode){
    for(let i=0;i<orders.length;i++){
      console.log('打印新订单',orders[i])
      let content =  this._orderContent(orders[i])
      let orderId = ''+new Date().getTime()
      Print.toPrint(machineCode,orderId,content)
      let newOrder = {print_status:true}
      this._updateOrder(orders[i]._id, newOrder)
    }
  },
  _updateOrder:function(orderId, order){
    console.log('orderid',orderId)
    Order.updateOrder(orderId, order, res=>{
      // console.log('更新打印状态成功',res)
    })
  },
  // 调用云函数获取openId
  _getOpenId:function(){
    let openId = App.globalData.openId
    this.setData({openId})
    return openId
  },
  
  
  // 获取打印机信息
  _getPrinter:async function(){
    let res = await Printer.getPrinter({})
    console.log('printer',res)
    this.setData({
      printer:res[0]
    })
  },
  _classifyOrders:function(orders){
    let doingOrder = []
    let selfGetOrder = []
    let takeOutOrder = []
    let completedOrder = []
    for(var i in orders){
      switch(orders[i].order_status){
        case 0:
          doingOrder.push(orders[i])
          break;
        case 1:
          {
            if(orders[i].order_type == 1){
              selfGetOrder.push(orders[i])
            }else if(orders[i].order_type == 2){
              takeOutOrder.push(orders[i])
            }
            break;
          }
        case 2:
          completedOrder.push(orders[i])
          break;
        default:
          break
      }
    }
    this.setData({
      doingOrder: doingOrder,
      selfGetOrder: selfGetOrder,
      takeOutOrder:takeOutOrder,
      completedOrder: completedOrder
    })
    console.log('doing',doingOrder,selfGetOrder,takeOutOrder,completedOrder)
  },
  // 根据订单状态更新订单
  submitBtn:async function(e){
    let status = parseInt(e.detail.orderStatus)
    let order = {
      order_status:parseInt(e.detail.orderStatus) + 1
    }
    // 如果订单状态为：未完成状态 点击按钮则更新订单状态 status = status + 1
    if(status <= 2){
      await Order.updateOrder(order, e.detail.orderId)
      this.onShow()
    }
    // 如果订单状态为：已完成 则删除订单
    if(status == 3){
      await Order.deleteOrder(e.detail.orderId)
      this.onShow()
    }
  },
  // 订单详情
  orderDetail:function (e) {
    let orderId = e.detail.orderId
    wx.navigateTo({
      url: '/pages/orderDetail/orderDetail?orderId='+orderId,
    })
  },
  // 生成订单小票格式
  _orderContent:function(orderData){
    let date = ''+new Date().getTime()
    let orderNumber = date.substring(9)
    var strA = '.';
    var strB = '*';
    var content = "<FS2><center>#订单号："+orderNumber+"</center></FS2>";
    content += "<FS2><center>*微信小程序*</center></FS2>";
    content += "<FS2><center>*摇滚炒鸡*</center></FS2>";
    content += strA.repeat( 32);
    content += "<FS2><center>--在线支付--</center></FS2>";
    
    content += "订单时间:"+myDate.getDate(orderData.create_time)+"\n";
    content += "订单编号:"+orderData.order_id+"\n";
    content += strB.repeat( 14) + "商品" + strB.repeat( 14);
    content += "<table>";
    for(let i=0;i<orderData.products.length;i++){
      content += "<tr><td>"+orderData.products[i].product_name+"</td><td>"+orderData.products[i].product_count+"</td><td>"+orderData.products[i].product_sell_price+"</td></tr>";
    }
    content += "</table>";
    content += strA.repeat( 32);
    content += "<QR>摇滚炒鸡</QR>";
    content += "小计:￥"+orderData.total_price+"\n";
    content += "折扣:￥0 \n";
    content += strB.repeat(32);
    content += "<FS2>订单总价:￥"+orderData.total_price+"</FS2>\n";
    content += strB.repeat(32);
    content += "<FS2>"+orderData.address.detail+"</FS2>\n";
    content += "<FS2>手机号：</FS2>\n";
    content += "<FS2>"+orderData.address.telephone+"</FS2>\n";
    content += "<FS2>预定时间: </FS2>\n";
    content += "<FS2>"+orderData.arrive_time+"</FS2>\n";
    content += strA.repeat( 32);
    content += "<FS2>备注："+orderData.comment+"</FS2>\n";
    content += "<FS2><center>**#1 完**</center></FS2>";
  
    return content
},
  selfget:async function(){
    let address={
      telephone:'13253377401',
      name:'张彪'
    }
    let products=[
      {
        product_name:'土豆丝'
      },
      {
        product_name:'娃娃菜'
      },
      {
        product_name:'咸鸭蛋'
      }
    ]
    let order ={
      address:address,
      products:products,
      order_status:0,
      order_type:2,
      create_time:new Date()
    }
    let result = await Order.createOrder(order)
    console.log('create',result)
  },
  takeout: async function(){
    let address={
     
    }
    let products=[
      {
        product_name:'土豆丝'
      },
      {
        product_name:'娃娃菜'
      },
      {
        product_name:'咸鸭蛋'
      }
    ]
    let order ={
      address:address,
      products:products,
      order_status:0,
      order_type:1,
      customer_telephone:13253377401,
      customer_name:'张彪',
      create_time:new Date()
    }
    let result = await Order.createOrder(order)
    console.log('create',result)
  },

  // 打印订单
  printOrder:async function(e){
    console.log('print')
    let order = {
      _id : e.detail.orderId
    }
    let res = await Order.getOrder(order)
    let content =  this._orderContent(order)
    let machineCode = this.data.printer.machine_code
    Print.toPrint(machineCode,res[0].order_id,content)
  },
 
})
