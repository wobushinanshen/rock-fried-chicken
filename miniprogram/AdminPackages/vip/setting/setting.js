import {ShopModel} from '../../../models/ShopModel.js'
let Shop = new ShopModel()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        shop:{},
        couponCount:0,
        couponMoney:0,
        vipFee:0,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this._init()
    },
    _init:function(){
        this._getShop()
    },
    _getShop:async function(){
        let result = await Shop.getShop({})
        if(result.length > 0){
            this.setData({
                shop:result[0],
                couponMoney:result[0].coupon_money,
                couponCount:result[0].coupon_count,
                vipFee:result[0].vip_fee
            })
        }else{
            wx.showToast({
              title: '数据读取失败!',
              icon:'none'
            })
        }
    },
    _inputCount:function(e){
        let couponCount = e.detail
        this.setData({couponCount})
    },
    _inputMoney:function(e){
        let couponMoney = e.detail
        this.setData({couponMoney})
    },
    _inputVipFee:function(e){
        let vipFee = e.detail
        this.setData({vipFee})
    },
     // 校验数字
     _isNumber:function(number){
        var reg = /^[1-9][0-9]*$/
        if(!reg.test(number)){
            return false
        }else{
            return true
        }
    },
    _isEmpty:function(str){
        if(str == '' || str == null){
            return true
        }else{
            return false
        }
    },
    _checkSubmit:function(){
        if(this._isEmpty(this.data.couponCount)){
            return '优惠券数量不能为空!'
        }
        if(!this._isNumber(this.data.couponCount)){
            return '优惠券数量应为正整数!'
        }
        if(this._isEmpty(this.data.couponMoney)){
            return '优惠券金额不能为空!'
        }
        if(!this._isNumber(this.data.couponMoney)){
            return '优惠券金额应为正整数!'
        }
        if(this._isEmpty(this.data.vipFee)){
            return '会员费金额不能为空!'
        }
        if(!this._isNumber(this.data.vipFee)){
            return '会员费金额应为正整数!'
        }
        return ''
    },
    cancel:function(){
        wx.navigateBack({
          delta: 1,
        })
    },
    confirm:async function(){
        let that = this
        let message = that._checkSubmit()
        if(message !== ''){
            wx.showToast({
              title: message,
              icon:'none'
            })
        }else{
            let shop = {
                coupon_count:parseInt(that.data.couponCount),
                coupon_money:parseInt(that.data.couponMoney),
                vip_fee:parseInt(that.data.vipFee)
            }
            let result = await Shop.updateShop(shop, this.data.shop._id)
            that.cancel()
        }
    },

  
})