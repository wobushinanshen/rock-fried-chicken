import { CloudRequest } from '../request/index-request.js'
class CategoryModel extends CloudRequest {
    /**
     * 获取分类
     * @param {*} callBack 
     */
    async getCategory(category){
        let res = await this.request({
            url: "getCategory",
            data:{category:category}
        })
        return res.result.data.data
    }
    
    async createCategory(category){
        let res = await this.request({
            url: "createCategory",
            data:{category:category},
            success: res => {
              callBack(res)
            }
        })
        return res.result.data.data
    }

    async deleteCategory(categoryId){
        let res = await this.request({
            url: "deleteCategory",
            data:{categoryId:categoryId}
        })
        return res.result.data.data
    }

    async updateCategory(category, categoryId){
        let res = await this.request({
            url: "updateCategory",
            data:{
                categoryId:categoryId,
                category:category
            }
        })
        return res.result.data.data
    }

}
export { CategoryModel }