import { CloudRequest } from '../request/customer-request.js'
class CustomerModel extends CloudRequest {
    /**
     * 获取商家账户信息
     * 
     */
    async getCustomer(customer) {
        let res = await  this.request({
            url: "getCustomer",
            data:{
                customer:customer
            }
        })
        return res.result.data.data
    }
    async updateCustomer(customer, customerId){
        let res = await  this.request({
            url:"updateCustomer",
            data:{
                customer:customer,
                customerId:customerId
            }
        })
        return res.result.data.data
    }
    async createCustomer(customer){
        let res = await  this.request({
            url:"addCustomer",
            data:{
                customer:customer,
            }
        })
        return res.result.data.data
    }
    async deleteCustomer(customerId){
        let res = await  this.request({
            url:"delCustomer",
            data:{
                customerId:customerId
            },
        })
        return res.result.data.data
    }

    
}

export { CustomerModel }
