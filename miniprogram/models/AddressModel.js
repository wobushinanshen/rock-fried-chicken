
import { CloudRequest } from '../request/customer-request.js'
class AddressModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  
  async getAddress(address) {
    let res = await this.request({
      url: "getAddress",
      data:{address:address}
    })
    return res.result.data.data
  }
 
  async createAddress(address){
    let res = await this.request({
        url: "createAddress",
        data:{
          address:address
        }
      })
    return res.result.data.data
  }

  async updateAddressColumn(address, options){
    let res = await this.request({
        url: "resetAddress",
        data:{
          address:address,
          options:options
        }
      })
    return res.result.data.data
  }

  async updateAddress(address, addressId){
    let res = await this.request({
        url: "updateAddress",
        data:{
          address : address,
          addressId : addressId
        }
      })
    return res.result.data.data
  }
  async deleteAddress(addressId){
    let res = await this.request({
        url: "deleteAddress",
        data:{addressId:addressId}
      })
    return res.result.data.data
  }

  

}
export { AddressModel }
