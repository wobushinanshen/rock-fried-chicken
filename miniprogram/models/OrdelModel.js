import { CloudRequest } from '../request/customer-request.js'
class OrderModel extends CloudRequest {

    /**
     * 生成订单
     * @param {*} orderData 
     * @param {*} callBack 
     */
    async createOrder(order) {
        let res = await  this.request({
            url: "createOrder",
            data: { order: order }
        })
        return res.result.data.data
    }

    /**
     * 查询订单
     * @param {*} callBack 
     */
   async getOrder(order) {
       let res = await this.request({
            url: "getOrder",
            data:{order:order}
        })
        return res.result.data.data
    }

    async deleteOrder(orderId){
        let res = await  this.request({
            url: "deleteOrder",
            data:{orderId:orderId}
        })
        return res.result.data.data
    }
    async updateOrder(order, orderId){
        let res = await  this.request({
            url:"updateOrder",
            data:{
                orderId:orderId,
                order:order
            }
        })
        return res
    }


}

export { OrderModel }