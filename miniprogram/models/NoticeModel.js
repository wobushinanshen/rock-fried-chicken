
import { CloudRequest } from '../request/admin-request.js'
class NoticeModel extends CloudRequest {

  async getNotice(notice) {
    let res = await this.request({
      url: "getNotice",
      data:{notice:notice}
    })
    return res.result.data.data
  }
  async getNoticeById(noticeId) {
    let res = await this.request({
      url: "getNoticeById",
      data:{noticeId:noticeId}
    })
    return res.result.data.data
  }

  async updateNotice(notice, noticeId) {
    let res = await this.request({
      url: "updateNotice",
      data:{
        notice:notice,
        noticeId:noticeId
      }
    })
    return res.result.data.data
  }
  async delNotice(noticeId){
    let res = await this.request({
        url: "delNotice",
        data:{noticeId:noticeId}
      })
    return res.result.data.data
  }
 
}
export { NoticeModel }
