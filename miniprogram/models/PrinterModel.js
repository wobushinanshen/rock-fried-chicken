
import { CloudRequest } from '../request/admin-request.js'
class PrinterModel extends CloudRequest {
  
  
  async getPrinter(printer) {
    let res = await this.request({
      url: "getPrinter",
      data:{printer:printer}
    })
    return res.result.data.data
  }
  async updatePrinter(printer, printerId) {
    let res = await this.request({
      url: "updatePrinter",
      data:{
          printer : printer,
          printerId: printerId
        }
    })
    return res.result.data.data
  }
  async deletePrinter(printerId) {
    let res = await this.request({
      url: "deletePrinter",
      data:{printerId:printerId}
    })
    return res.result.data.data
  }


 
}
export { PrinterModel }
