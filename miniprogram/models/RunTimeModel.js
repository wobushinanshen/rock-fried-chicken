import { CloudRequest } from '../request/admin-request.js'
class RunTimeModel extends CloudRequest {
    /**
     * 获取商家账户信息
     * 
     */
    async getRunTime(runtime) {
        let res = await  this.request({
            url: "getRunTime",
            data:{runtime:runtime}
        })
        return res.result.data.data
    }
    async updateRunTime(runTime, runTimeId){
        let res = await  this.request({
            url:"updateRunTime",
            data:{
                runTime:runTime,
                runTimeId:runTimeId
            }
        })
        return res.result.data.data
    }
    async createRunTime(runtime){
        let res = await  this.request({
            url:"createRunTime",
            data:{
                runtime:runtime,
            }
        })
        return res.result.data.data
    }
    async deleteRunTime(runTimeId){
        let res = await  this.request({
            url:"deleteRunTime",
            data:{
                runTimeId:runTimeId,
            }
        })
        return res.result.data.data
    }
    // 判断商店是否开门
    async checkShopOpen(){
        let res = await this.getRunTime({})
        // begin_time 形如：08:00
        let begin_time = parseInt(res[0].begin_time.replace(':',''))//将时间转换为数字
        let end_time = parseInt(res[0].end_time.replace(':',''))
        let date = new Date()
        let hour = date.getHours()  
        let minute =  date.getMinutes()
        let now = hour * 100 + minute
        console.log('now',now,begin_time,end_time)
        if(now >= begin_time && now <= end_time){
            return true
        }else{
            return false
        }
    }
    
}

export { RunTimeModel }
