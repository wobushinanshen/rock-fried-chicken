import { CloudRequest } from '../request/admin-request.js'
class ShopModel extends CloudRequest {
    /**
     * 获取商家账户信息
     * 
     */
    async getShop(shop) {
        let res = await this.request({
            url: "getShop",
            data:{shop:shop},
        })
        return res.result.data.data
    }
    async updateShop(shop, shopId){
        let res = await this.request({
            url:"updateShop",
            data:{
                shop:shop,
                shopId:shopId
            }
        })
        return res.result.data.data
    }
    async createShop(shop){
        let res =  await this.request({
            url:"addShop",
            data:{
                shop:shop,
            }
        })
        return res.result.data.data
    }

    
}

export { ShopModel }
