
import { CloudRequest } from '../request/index-request.js'
class ProductModel extends CloudRequest {
  

  /**
   * 模糊查询
   * @param {*} regexp 
   * @param {*} callBack 
   */
  async regQuery(regexp, columns) {
    let res = await this.request({
      url: "regQuery",
      data:{
        regexp:regexp,
        columns:columns
      }
    })
    return res.result.data.data
  }


  async createProduct(product){
    let res = await this.request({
      url: "createProduct",
      data: {product:product}
    })
    return res.result.data.data
  }

  async getProduct(product){
    let res = await this.request({
      url: "getProduct",
      data:{product:product}
    })
    return res.result.data.data
  }

  async updateProduct(product,productId){
    let res = await this.request({
      url: "updateProduct",
      data: {product:product,
            productId: productId
      }
    })
    return res.result.data.data
  }

  async deleteProduct(productId){
    let res = await this.request({
      url: "deleteProduct",
      data: { productId: productId}
    })
    return res.result.data.data
  }


  

}
export { ProductModel }
