
import { CloudRequest } from '../request/login-request.js'
class LoginModel extends CloudRequest {
  
  /**
   * 根据商品ID 获取商品信息
   * @param {*} product_id 
   * @param {*} callBack 
   */
  
  async getAddress(address) {
    let res = await this.request({
      url: "getAddress",
      data:{address:address}
    })
    return res.result.data.data
  }

  async getOpenId() {
    let res = await this.request({
      url: "getOpenId",
      data:'e'
    })
    return res
  }

  async getTelephone(param) {
    let res = await this.request({
      url: "getTelephone",
      data:param
    })
    return res
  }
 
 

  

}
export { LoginModel }
