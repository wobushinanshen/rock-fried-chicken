import { CloudRequest } from '../request/index-request.js'
class BannerModel extends CloudRequest {
    /**
     * 获取分类
     * @param {*} callBack 
     */
    async getBanner(banner){
        let res = await this.request({
            url: "getBanner",
            data:{banner:banner}
        })
        return res.result.data.data
    }

    async createBanner(banner){
        let res = await this.request({
            url: "addBanner",
            data:{banner:banner}
        })
        return res.result.data.data
    }

    async deleteBanner(bannerId){
        let res = await this.request({
            url: "deleteBanner",
            data:{bannerId:bannerId}
        })
        return res.result.data.data
    }

}
export { BannerModel }