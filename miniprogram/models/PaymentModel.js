import { CloudRequest } from '../request/payment-request.js'
class PaymentModel extends CloudRequest{
    // 统一下单
    unifiedOrder(order ,callBack){
        this.request({
            url:'unifiedOrder',
            data:{order:order},
            success: res => {
                callBack(res)
            }
        })
    }

    // 查询订单
    queryOrder(order, callBack){
        this.request({
            url:'queryOrder',
            data:{order:order},
            success: res => {
                callBack(res)
            }
        })
    }

    // 关闭订单
    closeOrder(order, callBack){
        this.request({
            url:'closeOrder',
            data:{order:order},
            success: res => {
                callBack(res)
            }
        })
    }

     // 申请退款
     refund(order, callBack){
        this.request({
            url:'refund',
            data:{order:order},
            success: res => {
                callBack(res)
            }
        })
    }

     // 退款查询
     queryRefund(order, callBack){
        this.request({
            url:'queryRefund',
            data:{order:order},
            success: res => {
                callBack(res)
            }
        })
    }



}
export { PaymentModel }