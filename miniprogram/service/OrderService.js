import { OrderModel } from '../models/OrderModel.js'
let Order =  new OrderModel()
let App = getApp()
// 查询订单
async function getOrder(order){
    let res = await Order.getOrder(order)
    let results = res.result.data.data 
    return results
}
// 创建订单
async function createOrder(order){
    let res = await Order.createOrder(order)
    let result = res.result.data.data 
    return result
}
// 更新订单
async function updateOrder(order, orderId){
    let res = await Order.updateOrder(order, orderId)
    let result = res.result.data.data 
    return result
}
// 删除订单
async function deleteOrder(order){
    let res = await Order.deleteOrder(order)
    let result = res.result.data.data 
    return result
}


module.exports = {
    createOrder,
    getOrder,
    updateOrder,
    deleteOrder

}
