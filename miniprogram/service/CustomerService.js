import { CustomerModel } from '../models/CustomerModel.js'
let Customer =  new CustomerModel()
let App = getApp()

async function getCustomer(customer){
    let res = await Customer.getCustomer(customer)
    let result = res.result.data.data 
    return result
}

// 创建订单
async function createCustomer(customer){
    let res = await Customer.createCustomer(customer)
    let result = res.result.data.data 
    return result
}
// 更新订单
async function updateCustomer(customer, customerId){
    let res = await Customer.updateCustomer(customer, customerId)
    let result = res.result.data.data 
    return result
}
// 删除订单
async function deleteCustomer(customer){
    let res = await Customer.deleteCustomer(customer)
    let result = res.result.data.data 
    return result
}
module.exports = {
    getCustomer,
    createCustomer,
    updateCustomer,
    deleteCustomer
}
