import { ShopModel } from '../models/ShopModel.js'
import { RunTimeModel } from '../models/RunTimeModel.js'
let Shop =  new ShopModel()
let RunTime =  new RunTimeModel()
let App = getApp()
/*********************  订单  ********************/
//  获取订单
async function getShop(shop){
    let res = await Shop.getShop(shop)
    let result = res.result.data.data 
    return result
}
// 创建订单
async function createShop(shop){
    let res = await Shop.createShop(shop)
    let result = res.result.data.data 
    return result
}
// 更新订单
async function updateShop(shop, shopId){
    let res = await Shop.updateShop(shop, shopId)
    let result = res.result.data.data 
    return result
}
// 删除订单
async function deleteShop(shopId){
    let res = await Shop.deleteShop(shopId)
    let result = res.result.data.data 
    return result
}

/*********************  营业时间  ********************/

//  获取营业时间
async function getRunTime(runtime){
    let res = await RunTime.getRunTime(runtime)
    let result = res.result.data.data 
    return result
}
// 创建营业时间
async function createRunTime(runtime){
    let res = await RunTime.createRunTime(runtime)
    let result = res.result.data.data 
    return result
}
// 更新营业时间
async function updateRunTime(runtime){
    let res = await RunTime.updateRunTime(runtime)
    let result = res.result.data.data 
    return result
}
// 删除营业时间
async function deleteRunTime(runtime){
    let res = await RunTime.deleteRunTime(runtime)
    let result = res.result.data.data 
    return result
}

module.exports = {
    getShop,
    createShop,
    updateShop,
    deleteShop,
    getRunTime,
    createRunTime,
    updateRunTime,
    deleteRunTime

}
