// components/x-coupon/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        coupon:Object,
        disabled:Boolean,
        radio:Boolean,
        none:Boolean,
        checked:Boolean,
        
    },

    /**
     * 组件的初始数据
     */
    data: {
        checked:false,
        disabled:false,
        none:true,
        radio:false//默认不显示单选框 只有当radio为1时才显示
    },

    /**
     * 组件的方法列表
     */
    methods: {
        showDetail:function(){
            this.triggerEvent('showDetail',{couponId:this.data.coupon._id},{})
        },
        selectRadio:function(e){
            this.triggerEvent('selectRadio',{couponId:this.data.coupon._id},{})

        }
    }
})
