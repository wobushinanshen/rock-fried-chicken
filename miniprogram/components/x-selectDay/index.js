// components/x-selectTime/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        beginTime:String,
        endTime:String
    },

    /**
     * 组件的初始数据
     */
    data: {
        beginShow:false,
        endShow:false,
        currentDate: new Date().getTime(),
        minDate: new Date().getTime(),
        formatter(type, value) {
          if (type === 'year') {
            return `${value}年`;
          } else if (type === 'month') {
            return `${value}月`;
          }
          return value;
        },
    },

    /**
     * 组件的方法列表
     */
    methods: {
    // 开始时间
    showBeginPopup() {
        this.setData({ beginShow: true });
      },
    beginInput:function(event){
        this.setData({
            currentDate:event.detail
        })
    },
    endInput:function(event){
        this.setData({
            currentDate:event.detail
        })
    },
    beginConfirm(event) {
        this.setData({ 
          beginShow: false ,
          currentDate: event.detail,
        });
        this.triggerEvent('beginConfirm',{beginTime:this.data.beginTime},{})
      },
    beginCancel() {
        this.setData({ beginShow: false });
      },
  
      // 结束时间
    showEndPopup() {
        this.setData({ endShow: true });
      },
    endConfirm(event) {
        this.setData({ 
          endShow: false ,
          currentDate: event.detail,
        });
        this.triggerEvent('endConfirm',{endTime:this.data.endTime},{})
      },
    endCancel() {
        this.setData({ endShow: false });
      },
    }
})
