// components/product-column/index.js
let productBehavior = require('../behaviors/product-behavior.js')
Component({
  /**
   * 组件的属性列表
   */

  properties: {
    product:Object
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    modifyProduct:function(){
      this.triggerEvent('modify', {productId:this.data.product._id}, {})
    },
    deleteProduct:function(){
      this.triggerEvent('delete', {product:this.data.product}, {})
    }


  }
})
