// components/x-address/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        address:Object
    },

    /**
     * 组件的初始数据
     */
    data: {
        checked: true,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onChange(event) {
            this.setData({
              checked: event.detail,
            })
        },
        edit:function(){
            this.triggerEvent('editAddress',{addressId:this.data.address._id},{})
        },
        delete:function(){
            this.triggerEvent('deleteAddress',{addressId:this.data.address._id},{})

        },
        changeRadio:function(){
            this.triggerEvent('setDefaultAddress',{addressId:this.data.address._id},{})
        }
        
    }
})
