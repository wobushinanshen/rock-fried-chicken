// components/x-runTime/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        runTime:Object
    },

    /**
     * 组件的初始数据
     */
    data: {
        
    },

    /**
     * 组件的方法列表
     */
    methods: {
        editTime:function(){
            let timeId = this.data.runTime._id
            this.triggerEvent('editTime',{timeId:timeId},{})
            
        },
        deleteTime:function(){
            let timeId = this.data.runTime._id
            this.triggerEvent('deleteTime',{timeId:timeId},{})
        },

    }
})
