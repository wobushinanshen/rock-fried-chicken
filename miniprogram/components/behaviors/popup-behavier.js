module.exports = Behavior({
    behaviors: [],
    properties: {
      product: { // 属性名
        type: Object,
        value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
        observer: function (category) {
        }
      },
      show:{
          type:Boolean
      },
      count:{
          type:Number,
          value:1
      }
    },
    methods: {
      setCount:function(e){
        this.setData({
            count:e.detail
        })
      },
      onClose:function(){
        let show = false
        this.setData({show})
      },
      joinCart:function(){
        this.triggerEvent(
            'joinCart', {product:this.data.product,count:this.data.count}, {}
        )
      },
      buy:function(){
        this.triggerEvent(
            'buy', {product:this.data.product,count:this.data.count}, {}
        )
      },


    }
  })
  
  