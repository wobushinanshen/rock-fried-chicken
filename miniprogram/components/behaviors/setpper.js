module.exports = Behavior({
    behaviors: [],
    properties: {
      order: { // 属性名
        type: Object,
        value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
        observer: function (category) {
        }
      },
      identity:{
        type: Boolean//身份 若真则为商家 否则为顾客
      }
    },
    methods: {
      // 商品详情
      orderDetails: function () {
        this.triggerEvent("orderDetails", {
          orderId: this.data.order._id
        }, {})
      },
      delOrder: function () {
        this.triggerEvent("delOrder", {
          orderId: this.data.order._id
        }, {})
      }
    }
  })
  
  