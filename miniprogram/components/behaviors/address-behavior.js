module.exports = Behavior({
    behaviors: [],
    properties: {
      address: { // 属性名
        type: Object,
        value: '', // 属性初始值（可选），如果未指定则会根据类型选择一个
        observer: function (category) {
        }
      }
    },
    methods: {
      // 商品详情
      addressDetails: function () {
        // 返回 
        this.triggerEvent("addressDetails", {
          addressId: this.data.address._id
        }, {})
      },
      selectAddress:function(){
        this.triggerEvent("selectAddress", {
            addressId: this.data.address._id
          }, {})
      }
    }
  })
  
  