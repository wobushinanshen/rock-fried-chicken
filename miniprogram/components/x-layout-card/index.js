// components/x-layout-card/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
       bgColor:{
           type:String,
           observer(color){
            let style = ''
            if(color){
                style = `--bg-color:${color};`
            }
            this.setData({
                baseStyle:style
            })
           }
       },
       icon:String,
       titleZh:String,
       titleEn:String,
    },

    /**
     * 组件的初始数据
     */
    data: {
        // baseStyle:`--bg-color:#2ec1b9;`
        
    },

    /**
     * 组件的方法列表
     */
    methods: {
        navigateTo:function(){
            this.triggerEvent('navigateTo',{},{})
        }
    }
})
