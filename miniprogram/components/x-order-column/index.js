// components/order/order.js

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        order:Object,
        identity:Boolean
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        orderDetails: function () {
            this.triggerEvent("orderDetails", {
              orderId: this.data.order._id
            }, {})
          },
          delOrder: function () {
            this.triggerEvent("delOrder", {
              orderId: this.data.order._id
            }, {})
          }
    }
})
