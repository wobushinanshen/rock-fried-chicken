// components/upload-file/upload-file.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
    
    },

    /**
     * 组件的初始数据
     */
    data: {
      fileList: Array,
        image:false,
        filePath: "",
        


    },

    /**
     * 组件的方法列表
     */
    methods: {
    _uploadImg:function(evt){
        var that =this;
        wx.chooseImage({
          count: 0,
          success:res=>{
            let  filePath=res.tempFilePaths[0];
              that.setData({
                filePath:filePath,
                image:true
              })
              this.triggerEvent('uploadImg',{filePath:filePath},{})
            //console.log('success',filePath)
          }
        })
        //let path = that.filePath
        
    },
    _handlerDelete:function(){
      this.setData({
        image:false,
        filePath:""
      })
      this.triggerEvent('delImg', {filePath:""}, {})
    }
    }
})
