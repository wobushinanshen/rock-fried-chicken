// components/x-order/x-paid-order/index.js
import {  OrderModel } from '../../../models/OrdelModel.js'
let Order = new OrderModel()
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        order:Object,
        identity:Boolean
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        // 管理员或顾客 ：确认接单
        confirmAccepted:function(){
            this.triggerEvent("confirmAccepted", {
                orderId: this.data.order._id
              }, {})
        },
        orderDetail:function () {
            this.triggerEvent("orderDetail", {
                orderId: this.data.order._id
              }, {})
        },
        printOrder:function(){
            this.triggerEvent("printOrder", {
              orderId: this.data.order._id
            }, {})
          }

    }
})
