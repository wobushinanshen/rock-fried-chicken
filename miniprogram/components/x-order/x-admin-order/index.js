// components/x-order/x-admin-order/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        order:{
           type:Object,
           observer(order){
            let style = ''
            if(order.order_type == 2){
                style = `--bg-color:#1296db;`
            }else{
                style = `--bg-color:#d81e06;`
            }
            this.setData({
                baseStyle:style
            })
           } 
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        baseStyle:''
    },

    /**
     * 组件的方法列表
     */
    methods: {
        // 联系顾客 拨打电话
        call:function(){
           
            let telephone = ''
            if(this.data.order.order_type == 1){
                telephone = this.data.order.customer_telephone
            }else{
                telephone = this.data.order.address.telephone
            }
            wx.makePhoneCall({
              phoneNumber: telephone,
            })
        },
        // 确认按钮
        submit:function(){
            this.triggerEvent('submit', {orderId:this.data.order._id,orderStatus:this.data.order.order_status}, {})
        },
        // 打印订单
        printOrder:function(){
            this.triggerEvent('printOrder', {order:this.data.order}, {})
        },
        // 展示订单详情
        showDetail:function(){
            this.triggerEvent('showDetail', {orderId:this.data.order._id}, {})
        }
    }
})
