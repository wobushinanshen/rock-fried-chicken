// components/x-selectTime/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        beginTime:String,
        endTime:String
    },

    /**
     * 组件的初始数据
     */
    data: {
        beginShow:false,
        endShow:false
    },

    /**
     * 组件的方法列表
     */
    methods: {
    // 开始时间
    showBeginPopup() {
        this.setData({ beginShow: true });
      },
    beginConfirm(event) {
        this.setData({ 
          beginShow: false ,
          beginTime: event.detail,
        });
        this.triggerEvent('beginConfirm',{beginTime:this.data.beginTime},{})
      },
    beginCancel() {
        this.setData({ beginShow: false });
      },
  
      // 结束时间
    showEndPopup() {
        this.setData({ endShow: true });
      },
    endConfirm(event) {
        this.setData({ 
          endShow: false ,
          endTime: event.detail,
        });
        this.triggerEvent('endConfirm',{endTime:this.data.endTime},{})
      },
    endCancel() {
        this.setData({ endShow: false });
      },
    }
})
