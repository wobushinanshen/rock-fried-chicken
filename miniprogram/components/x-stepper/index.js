// components/x-stepper/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        count:Number,

    },

    /**
     * 组件的初始数据
     */
    data: {
     
    },

    /**
     * 组件的方法列表
     */
    methods: {
        addCount:function(e){
            this.setData({
                count:this.data.count+1
            })
            this.triggerEvent(
                'addCount',{count:this.data.count},{}
            )
        },
        subCount:function(){
            this.setData({
                count:this.data.count-1
            })
            this.triggerEvent(
                'subCount',{count:this.data.count},{}
            )
        },
        reset:function(){
            this.setData({
                count:0
            })
            this.triggerEvent(
                'reset',{count:this.data.count},{}
            )
        },

    }
})
