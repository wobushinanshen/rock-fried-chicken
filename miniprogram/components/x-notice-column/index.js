// components/notice-column/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        notice:Object
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        changeSwitch:function(e){

            this.setData({
                'notice.release':!this.data.notice.release
            })
        },
        changeField:function(e){
         
            this.setData({
                'notice.notice': e.detail
            })
        },
        cancel:function(){
            this.triggerEvent(
                'cancel',{},[]
            )
        },
        submit:function(){
            this.triggerEvent(
                'submit',{notice:this.data.notice},{}
            )
        }

    }
})
