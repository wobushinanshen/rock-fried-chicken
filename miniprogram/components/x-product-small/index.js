// components/x-cart/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        product:Object
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        addCount:function(event){
            let count = event.detail.count
            this.setData({
                'product.select_count':count
            })
            let product = {}
            product._id = this.data.product._id
            product.select_count = this.data.product.select_count
            product.product_name = this.data.product.product_name
            product.category_type = this.data.product.category_type
            product.product_sell_price = this.data.product.product_sell_price
            product.product_image = this.data.product.product_image
            this.triggerEvent(
                'addCount',{product:product},{}
            )
       
        },
        subCount:function(event){
            let count = event.detail.count
            this.setData({
                'product.select_count':count
            })
            let product = {}
            product._id = this.data.product._id
            product.product_name = this.data.product.product_name
            product.select_count = this.data.product.select_count
            product.category_type = this.data.product.category_type
            product.product_sell_price = this.data.product.product_sell_price
            product.product_image = this.data.product.product_image
            this.triggerEvent(
                'subCount',{product:product},{}
            )

        },

    }
})
