Component({
    /**
     * 组件的属性列表
     */
    properties: {
        category:Object
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        editCategory:function(){
            this.triggerEvent('editCategory', {category_id:this.data.category._id,category_name:this.data.category.category_name}, {})
        },
        deleteCategory:function(){
            this.triggerEvent('deleteCategory', {category_id:this.data.category._id}, {})
        }
    }
})
